create table if not exists "EVENT"
(
    id bigint not null
    constraint "EVENT_pkey"
    primary key,
    date timestamp,
    title varchar(255),

    );

alter table "EVENT" owner to postgres;