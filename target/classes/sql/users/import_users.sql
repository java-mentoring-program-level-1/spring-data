INSERT INTO public."USER"(id, email, name) VALUES (nextval('user-sequence'), 'jock@email.com', 'Jock');
INSERT INTO public."USER"(id, email, name) VALUES (nextval('user-sequence'), 'rick@email.com', 'Rick');
INSERT INTO public."USER"(id, email, name) VALUES (nextval('user-sequence'), 'tony@email.com', 'Tony');
INSERT INTO public."USER"(id, email, name) VALUES (nextval('user-sequence'), 'nemm@email.com', 'Nemm');
INSERT INTO public."USER"(id, email, name) VALUES (nextval('user-sequence'), 'ahon@email.com', 'Ahon');
