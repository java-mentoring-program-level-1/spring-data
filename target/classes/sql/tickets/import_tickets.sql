INSERT INTO public."TICKET"(id, category, place, eventid, userid)
    VALUES (nextval('ticket-sequence'), 'PREMIUM', 122, 5, 2);

INSERT INTO public."TICKET"(id, category, place, eventid, userid)
    VALUES (nextval('ticket-sequence'), 'STANDARD', 2, 5, 3);

INSERT INTO public."TICKET"(id, category, place, eventid, userid)
    VALUES (nextval('ticket-sequence'), 'BAR', 4, 1, 4);

INSERT INTO public."TICKET"(id, category, place, eventid, userid)
    VALUES (nextval('ticket-sequence'), 'PREMIUM', 9, 3, 5);

INSERT INTO public."TICKET"(id, category, place, eventid, userid)
    VALUES (nextval('ticket-sequence'), 'STANDARD', 65, 4, 1);

INSERT INTO public."TICKET"(id, category, place, eventid, userid)
    VALUES (nextval('ticket-sequence'), 'BAR', 32, 3, 1);

INSERT INTO public."TICKET"(id, category, place, eventid, userid)
    VALUES (nextval('ticket-sequence'), 'PREMIUM', 98, 2, 3);

INSERT INTO public."TICKET"(id, category, place, eventid, userid)
    VALUES (nextval('ticket-sequence'), 'PREMIUM', 27, 4, 3);

INSERT INTO public."TICKET"(id, category, place, eventid, userid)
    VALUES (nextval('ticket-sequence'), 'STANDARD', 31, 1, 2);
