DROP TABLE IF EXISTS public."USER_ACCOUNT" CASCADE;

CREATE TABLE IF NOT EXISTS public."USER_ACCOUNT"
(
    id bigint NOT NULL,
    amount numeric(19,2),
    userid bigint NOT NULL,
    CONSTRAINT "USER_ACCOUNT_pkey" PRIMARY KEY (id)
);

DROP SEQUENCE IF EXISTS public."user-account-sequence" CASCADE;

CREATE SEQUENCE IF NOT EXISTS public."user-account-sequence" INCREMENT BY 1 START WITH 1 OWNED BY public."USER_ACCOUNT".id;