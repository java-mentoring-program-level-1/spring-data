DROP TABLE IF EXISTS public."USER" CASCADE;

CREATE TABLE IF NOT EXISTS public."USER"
(
    id bigint NOT NULL,
    email character varying(255) COLLATE pg_catalog."default" NOT NULL,
    name character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT "USER_pkey" PRIMARY KEY (id),
    CONSTRAINT uk_oso07pudw19e66bs4yp8hwpux UNIQUE (email)
);

DROP SEQUENCE IF EXISTS public."user-sequence" CASCADE;

CREATE SEQUENCE IF NOT EXISTS public."user-sequence" INCREMENT BY 1 START WITH 1 OWNED BY public."USER".id;