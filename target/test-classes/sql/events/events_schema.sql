DROP TABLE IF EXISTS public."EVENT" CASCADE;

CREATE TABLE IF NOT EXISTS public."EVENT"
(
    id bigint NOT NULL,
    date timestamp without time zone NOT NULL,
    ticketprice numeric(19,2),
    title character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "EVENT_pkey" PRIMARY KEY (id),
    CONSTRAINT uk_mk8k1tps4rfjgbnslr8n21yma UNIQUE (title)
);

DROP SEQUENCE IF EXISTS public."event-sequence" CASCADE;

CREATE SEQUENCE IF NOT EXISTS public."event-sequence" INCREMENT BY 1 START WITH 1 OWNED BY public."EVENT".id;