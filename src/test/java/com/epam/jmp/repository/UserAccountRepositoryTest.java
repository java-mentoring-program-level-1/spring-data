package com.epam.jmp.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import com.epam.jmp.model.user.UserImpl;
import com.epam.jmp.model.user.account.UserAccountImpl;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = "/config/daos-test-context.xml")
@Sql(scripts = {"/sql/users/users_schema.sql", "/sql/users/import_users.sql",
                "/sql/accounts/accounts_schema.sql", "/sql/accounts/import_accounts.sql"
})
class UserAccountRepositoryTest {

  @Autowired
  private UserAccountRepository userAccountRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  EntityManager entityManager;


  @Test
  void shouldReturnNewAccountWhenCreateUserAccount() {
    UserImpl user = new UserImpl();
    user.setEmail("bloch@email.com");
    user.setName("Bloch");
    userRepository.save(user);

    UserAccountImpl userAccount = new UserAccountImpl();
    userAccount.setAmount(new BigDecimal("1500"));
    userAccount.setUser(user);
    UserAccountImpl result = userAccountRepository.save(userAccount);
    assertNotNull(result);
    assertEquals(user.getId(), result.getUser().getId());
  }

  @Test
  void shouldReturnAccountWhenFindByUserId() {
    Optional<UserAccountImpl> account = userAccountRepository.findByUserId(1L);
    assertTrue(account.isPresent());
  }

  @Test
  void shouldReturnEmptyWhenFindByUserId() {
    Optional<UserAccountImpl> account = userAccountRepository.findByUserId(0L);
    assertFalse(account.isPresent());
  }

  @Test
  @Transactional
  void shouldDeleteAccountWhenDeleteByUserId() {
    long userId = 1L;
    userAccountRepository.deleteByUserId(userId);
    boolean exist = userAccountRepository.existsByUserId(userId);
    assertFalse(exist);
  }

  @Test
  void shouldReturnTrueWhenExistsByUserId() {
    boolean exist = userAccountRepository.existsByUserId(1L);
    assertTrue(exist);
  }

  @Test
  void shouldReturnFalseWhenExistsByUserId() {
    boolean exist = userAccountRepository.existsByUserId(0L);
    assertFalse(exist);
  }
}
