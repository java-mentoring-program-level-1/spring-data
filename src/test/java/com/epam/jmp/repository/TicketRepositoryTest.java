package com.epam.jmp.repository;

import static com.epam.jmp.model.ticket.Ticket.Category.BAR;
import static com.epam.jmp.model.ticket.Ticket.Category.PREMIUM;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.epam.jmp.model.event.Event;
import com.epam.jmp.model.ticket.Ticket;
import com.epam.jmp.model.ticket.Ticket.Category;
import com.epam.jmp.model.ticket.TicketImpl;
import com.epam.jmp.model.user.User;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(value = "/config/daos-test-context.xml")
@Sql(scripts = {"/sql/events/events_schema.sql", "/sql/events/import_events.sql",
								"/sql/users/users_schema.sql", "/sql/users/import_users.sql",
								"/sql/tickets/tickets_schema.sql", "/sql/tickets/import_tickets.sql"
})
class TicketRepositoryTest {
	private static final int PAGE = 0;
	private static final int SIZE = 3;
	@Autowired
	TicketRepository ticketRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	EventRepository eventRepository;

	@Test
	void shouldReturnNewTicketWhenBookTicket() {
		TicketImpl ticket = makeTicket(1L, 1L, 12, PREMIUM);
		Ticket bookedTicket = ticketRepository.save(ticket);
		assertTicket(ticket, bookedTicket);
	}

	@Test
	void shouldThrowExceptionWhenBookTicketWithBookedPlace() {
		TicketImpl ticket = makeTicket(4, 3, 32, BAR);
		assertThrows(DataIntegrityViolationException.class, () -> ticketRepository.save(ticket));
	}

	@Test
	void shouldReturnTrueWhenCancelTicket() {
		long ticketId = 1L;
		ticketRepository.deleteById(ticketId);
		boolean exist = ticketRepository.existsById(ticketId);
		assertFalse(exist);
	}

	@Test
	void shouldThrowExceptionWhenCancelTicket() {
		assertThrows(EmptyResultDataAccessException.class, () -> ticketRepository.deleteById(0L));
	}

	@Test
	void shouldReturnBookedTicketsWhenGetBookedTickets() {
		User user = userRepository.getById(1L);
		Page<TicketImpl> tickets = ticketRepository.findByUser(user, PageRequest.of(PAGE, SIZE));
		assertFalse(tickets.isEmpty());
	}

	@Test
	void shouldReturnBookedTicketsWhenGetBookedTicketsWithEvent() {
		Event event = eventRepository.getById(2L);
		Page<TicketImpl> tickets =  ticketRepository.findByEvent(event, PageRequest.of(PAGE, SIZE));
		assertFalse(tickets.isEmpty());
	}

	private TicketImpl makeTicket(long userId, long eventId, int place, Category category) {
		TicketImpl ticket = new TicketImpl();
		ticket.setUser(userRepository.getById(userId));
		ticket.setEvent(eventRepository.getById(eventId));
		ticket.setPlace(place);
		ticket.setCategory(category);

		return ticket;
	}

	private void assertTicket(Ticket actualTicket, Ticket expectedTicket) {
		assertEquals(actualTicket.getEvent().getId(), expectedTicket.getEvent().getId());
		assertEquals(actualTicket.getUser().getId(), expectedTicket.getUser().getId());
		assertEquals(actualTicket.getPlace(), expectedTicket.getPlace());
		assertEquals(actualTicket.getCategory(), expectedTicket.getCategory());
	}
}
