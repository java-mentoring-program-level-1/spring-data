package com.epam.jmp.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.epam.jmp.model.event.EventImpl;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(value = "/config/daos-test-context.xml")
@Sql(scripts = {"/sql/events/events_schema.sql", "/sql/events/import_events.sql"})
class EventRepositoryTest {
  private static final int SIZE = 3;
  private static final int PAGE = 1;

  @Autowired
  SimpleDateFormat formatter;
  @Autowired
  EventRepository eventRepository;

  @Test
  void shouldReturnNewEventWhenCreateEvent() {
    EventImpl event = new EventImpl();
    event.setDate(new Date());
    event.setTitle("good event");
    event.setTicketPrice(new BigDecimal("35.50"));

    EventImpl result = eventRepository.save(event);

    assertEvent(event, result);
  }

  @Test
  void shouldThrowExceptionWhenCreateEventWithEmptyFields() {
    EventImpl event = new EventImpl();
    assertThrows(DataIntegrityViolationException.class,
      () -> eventRepository.save(event));
  }

  @Test
  void shouldReturnEventWhenGetEventById() {
    long eventId = 1L;
    Optional<EventImpl> eventOptional = eventRepository.findById(eventId);
    assertTrue(eventOptional.isPresent());
    assertEquals(eventId, eventOptional.get().getId());
  }

  @Test
  void shouldReturnEmptyEventWhenGetEventById() {
    long eventId = 100L;
    Optional<EventImpl> eventOptional = eventRepository.findById(eventId);
    assertFalse(eventOptional.isPresent());
  }

  @Test
  void shouldReturnUpdatedEventWhenUpdateEvent() throws ParseException {
    EventImpl event = new EventImpl();
    event.setId(1L);
    Date date = formatter.parse("12-11-2021 13:37:15");
    event.setDate(date);
    event.setTitle("today's boring event");

    EventImpl result = eventRepository.save(event);
    assertEvent(event, result);
  }

  @Test
  void shouldThrowExceptionWhenUpdateEventWithNullEvent() {
    assertThrows(InvalidDataAccessApiUsageException.class, () -> eventRepository.save(null));
  }

  @Test
  void shouldThrowExceptionWhenUpdateEventWithNullDate() {
    EventImpl event = new EventImpl();
    event.setTitle("tech world");

    assertThrows(DataIntegrityViolationException.class, () -> eventRepository.save(event));
  }

  @Test
  void shouldThrowExceptionWhenUpdateEventWithInvalidTitle() {
    EventImpl event = new EventImpl();
    event.setTitle("Java-z conference");
    event.setDate(new Date());

    assertThrows(DataIntegrityViolationException.class, () -> eventRepository.save(event));
  }

  @Test
  void shouldReturnEmptyWhenFindAllByDateBetween() throws ParseException {
    Page<EventImpl> events = eventRepository.findAllByDateBetween(formatter.parse("01-01-2032 00:00:00"),
      formatter.parse("31-12-2052 23:59:59"), PageRequest.of(PAGE, SIZE));

    assertNotNull(events);
    assertTrue(events.isEmpty());
  }

  @Test
  void shouldReturnEventsWhenFindAllByDateBetween() throws ParseException {
    Page<EventImpl> events = eventRepository.findAllByDateBetween(formatter.parse("01-01-2022 00:00:00"),
      formatter.parse("31-12-2022 23:59:59"), PageRequest.of(PAGE, SIZE));

    assertNotNull(events);
    assertEquals(SIZE, events.getSize());
    assertEquals(PAGE, events.getNumber());
    assertEquals(2, events.getTotalPages());
  }

  @Test
  void shouldReturnEmptyWhenFindEventsByTitle() {
    Page<EventImpl> events = eventRepository.findAllByTitle("no-title",
      PageRequest.of(PAGE, SIZE));

    assertNotNull(events);
    assertTrue(events.isEmpty());
  }

  @Test
  void shouldReturnEventsWhenFindEventsByTitle() {
    Page<EventImpl> events = eventRepository.findAllByTitle("z conference",
      PageRequest.of(PAGE, SIZE));

    assertNotNull(events);
    assertEquals(SIZE, events.getSize());
    assertEquals(PAGE, events.getNumber());
  }

  @Test
  void shouldReturnTrueWhenIsPresent() {
    boolean result = eventRepository.existsById(1L);
    assertTrue(result);
  }

  @Test
  void shouldReturnFalseWhenIsNotPresent() {
    boolean result = eventRepository.existsById(0L);
    assertFalse(result);
  }

  private void assertEvent(EventImpl actual, EventImpl expected) {
    assertEquals(actual.getId(), expected.getId());
    assertEquals(actual.getDate(), expected.getDate());
    assertEquals(actual.getTitle(), expected.getTitle());
    assertEquals(actual.getTicketPrice(), expected.getTicketPrice());
  }
}

