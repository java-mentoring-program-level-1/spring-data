package com.epam.jmp.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.epam.jmp.model.user.User;
import com.epam.jmp.model.user.UserImpl;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(value = "/config/daos-test-context.xml")
@Sql(scripts = {"/sql/users/users_schema.sql", "/sql/users/import_users.sql"})
class UserRepositoryTest {
	private static final int PAGE = 0;
	private static final int SIZE = 3;

	@Autowired
	UserRepository userRepository;

	@Test
	void shouldReturnNewUserWhenCreateUser() {
		UserImpl user = new UserImpl();
		user.setEmail("sergey@email.com");
		user.setName("Serge");

		UserImpl result = userRepository.save(user);
		assertUser(user, result);
	}


	@Test
	void shouldThrowExceptionWhenCreateUser() {
		UserImpl user = new UserImpl();
		assertThrows(DataIntegrityViolationException.class, () -> userRepository.save(user));
	}

	@Test
	void shouldThrowExceptionWhenCreateUserWithExistedEmail() {
		UserImpl user = new UserImpl();
		user.setEmail("jock@email.com");
		user.setName("albert einstein");
		assertThrows(DataIntegrityViolationException.class, () -> userRepository.save(user));
	}

	@Test
	void shouldReturnUserWhenGetUserById() {
		long userId = 5L;
		Optional<UserImpl> user = userRepository.findById(userId);
		assertTrue(user.isPresent());
		assertEquals(userId, user.get().getId());
	}

	@Test
	void shouldReturnEmptyWhenGetUserById() {
		long userId = 20L;
		Optional<UserImpl> user = userRepository.findById(userId);
		assertFalse(user.isPresent());
	}

	@Test
	void shouldReturnUserWhenGetUserByEmail() {
		String email = "tony@email.com";
		Optional<UserImpl> user = userRepository.findByEmail(email);
		assertTrue(user.isPresent());
		assertEquals(email, user.get().getEmail());

	}

	@Test
	void shouldReturnEmptyWhenGetUserByEmail() {
		String email = "otherUser@email.com";
		Optional<UserImpl> user = userRepository.findByEmail(email);
		assertFalse(user.isPresent());
	}

	@Test
	void shouldReturnUpdatedUserWhenUpdateUser() {
		UserImpl user = new UserImpl();
		user.setId(1L);
		user.setEmail("newMartin@email.com");
		user.setName("Martin");

		User result = userRepository.save(user);
		assertUser(user, result);
	}

	@Test
	void shouldThrowExceptionWhenUpdateUserWithExistedEmail() {
		UserImpl user = new UserImpl();
		user.setId(1L);
		user.setEmail("ahon@email.com");
		user.setName("Sam");
		assertThrows(DataIntegrityViolationException.class, () -> userRepository.save(user));
	}

	@Test
	void shouldThrowExceptionWhenUpdateUserWithNullValue() {
		assertThrows(InvalidDataAccessApiUsageException.class, ()-> {
			userRepository.save(null);
		});
	}


	@Test
	void shouldThrowExceptionWhenUpdateUserWithInvalidEmail() {
		UserImpl user = new UserImpl();
		user.setEmail(null);
		user.setName("seo");
		assertThrows(DataIntegrityViolationException.class, ()-> userRepository.save(user));
	}

	@Test
	void shouldReturnTrueWhenDeleteUser() {
		long userId = 1L;
		userRepository.deleteById(userId);
		boolean result = userRepository.existsById(userId);
		assertFalse(result);
	}

	@Test
	void shouldReturnFalseWhenDeleteUser() {
		long userId = 0L;
		assertThrows(EmptyResultDataAccessException.class, ()-> userRepository.deleteById(userId));
	}

	@Test
	void shouldReturnUsersWhenGetUsersByName() {
		String name = "Tony";
		Page<UserImpl> userPage = userRepository.findAllByName(name, PageRequest.of(PAGE, SIZE));
		assertFalse(userPage.isEmpty());
	}

	@Test
	void shouldReturnEmptyListWhenGetUsersByName() {
		String name = "Bobby";
		Page<UserImpl> userPage = userRepository.findAllByName(name, PageRequest.of(PAGE, SIZE));
		assertTrue(userPage.isEmpty());
	}

	@Test
	void shouldReturnTrueWhenIsPresent() {
		boolean result = userRepository.existsById(1L);
		assertTrue(result);
	}

	@Test
	void shouldReturnFalseWhenIsPresent() {
		boolean result = userRepository.existsById(0L);
		assertFalse(result);
	}

  private void assertUser(User actual, User expected) {
    assertEquals(actual.getId(), expected.getId());
    assertEquals(actual.getEmail(), expected.getEmail());
    assertEquals(actual.getName(), expected.getName());
  }
}
