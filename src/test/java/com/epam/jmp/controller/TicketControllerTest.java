package com.epam.jmp.controller;

import static com.epam.jmp.model.ticket.Ticket.Category.STANDARD;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.epam.jmp.model.ticket.Ticket.Category;

@ExtendWith(SpringExtension.class)
@SpringJUnitWebConfig
@ContextHierarchy({
  @ContextConfiguration(locations = "classpath:config/daos-test-context.xml"),
  @ContextConfiguration(locations = "classpath:config/services-context.xml"),
  @ContextConfiguration(locations = "classpath:config/application-context.xml")
})
@Sql(scripts = {"/sql/tickets/tickets_schema.sql", "/sql/tickets/import_tickets.sql",
                "/sql/events/events_schema.sql", "/sql/events/import_events.sql",
                "/sql/users/users_schema.sql", "/sql/users/import_users.sql",
                "/sql/accounts/accounts_schema.sql", "/sql/accounts/import_accounts.sql"
})
class TicketControllerTest {
  private static final String PAGE = "0";
  private static final String SIZE = "5";
  private static final String VIEW = "ticket.html";
  private static final String ATTRIBUTE_TICKET = "ticket";
  private static final String ATTRIBUTE_TICKET_PAGE = "ticketPage";

  private MockMvc mockMvc;

  @BeforeEach
  void setup(WebApplicationContext wac) {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
  }

  @Test
  void getTickets_userId_shouldReturnOk() throws Exception {
    // Given
    final String userId = "1";

    // When
    final ResultActions result = mockMvc.perform(get("/tickets/")
      .param("userId", userId)
      .param("page", PAGE)
      .param("size", SIZE));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attributeExists(ATTRIBUTE_TICKET_PAGE));
    result.andExpect(view().name(VIEW));
  }

  @Test
  void getTickets_notExistingUserId_shouldReturnNotFound() throws Exception {
    // Given
    final String userId = "-1";

    // When
    final ResultActions result = mockMvc.perform(get("/tickets")
      .param("userId", userId)
      .param("page", PAGE)
      .param("size", SIZE));

    // Then
    result.andDo(print());
    result.andExpect(status().isNotFound());
  }

  @Test
  void getTickets_eventId_shouldReturnOk() throws Exception {
    // Given
    final String eventId = "1";

    // When
    final ResultActions result = mockMvc.perform(get("/tickets/")
      .param("eventId", eventId)
      .param("page", PAGE)
      .param("size", SIZE));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attributeExists(ATTRIBUTE_TICKET_PAGE));
    result.andExpect(view().name(VIEW));
  }

  @Test
  void getTickets_notExistingEventId_shouldReturnNotFound() throws Exception {
    // Given
    final String eventId = "-1";

    // When
    final ResultActions result = mockMvc.perform(get("/tickets")
      .param("eventId", eventId)
      .param("page", PAGE)
      .param("size", SIZE));

    // Then
    result.andDo(print());
    result.andExpect(status().isNotFound());
  }

  @Test
  void getTicketsReport_userId_shouldReturnOkAndInputStream() throws Exception {
    // Given
    final String userId = "1";

    // When
    final ResultActions result = mockMvc.perform(get("/tickets/")
      .param("userId", userId)
      .param("page", PAGE)
      .param("size", SIZE)
      .accept(MediaType.APPLICATION_PDF));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(header().exists("Content-Disposition"));
    result.andExpect(header().string("Content-Type", MediaType.APPLICATION_PDF.toString()));
  }

  @Test
  void getTicketsReport_notExistingUserId_shouldReturnNotFound() throws Exception {
    // Given
    final String userId = "-1";

    // When
    final ResultActions result = mockMvc.perform(get("/tickets/")
      .param("userId", userId)
      .param("page", PAGE)
      .param("size", SIZE)
      .accept(MediaType.APPLICATION_PDF));

    // Then
    result.andExpect(status().isNotFound());
  }

  @Test
  void insertTicket_ticket_shouldReturnOkAndTicket() throws Exception {
    // Given
    final String eventId = "1";
    final String userId = "3";
    final String place = "16";
    final Category category = STANDARD;

    // When
    final ResultActions result = mockMvc.perform(post("/tickets/")
      .param("eventId", eventId)
      .param("userId", userId)
      .param("place", place)
      .param("category", category.name()));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attributeExists(ATTRIBUTE_TICKET));
    result.andExpect(view().name(VIEW));
  }

  @Test
  void insertTicket_ticketWithSameUserForSameEvent_shouldReturnBadRequest() throws Exception {
    // Given
    final String eventId = "3";
    final String userId = "1";
    final String place = "16";

    // When
    final ResultActions result = mockMvc.perform(post("/tickets/")
      .param("eventId", eventId)
      .param("userId", userId)
      .param("place", place)
      .param("category", "Standard"));

    // Then
    result.andExpect(status().isBadRequest());
  }

  @Test
  void insertTicket_ticketWithSamePlaceAndSameCategoryForSameEvent_shouldReturnBadRequest()
    throws Exception {

    // Given
    final String eventId = "3";
    final String userId = "2";
    final String place = "9";

    // When
    final ResultActions result = mockMvc.perform(post("/tickets/")
      .param("eventId", eventId)
      .param("userId", userId)
      .param("place", place)
      .param("category", "Premium"));

    // Then
    result.andExpect(status().isBadRequest());
  }

  @Test
  void cancelTicket_id_shouldReturnOk() throws Exception {
    // Given
    final String id = "2";

    // When
    final ResultActions result = mockMvc.perform(get("/tickets")
      .param("_method", "delete")
      .param("id", id));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attribute(
      "message", "Ticket with id = " + id + " is cancelled successfully"));
    result.andExpect(view().name(VIEW));
  }

  @Test
  void deleteEvent_notExistingId_shouldReturnOk() throws Exception {
    // Given
    final String id = "16345";

    // When
    final ResultActions result = mockMvc.perform(get("/tickets")
      .param("_method", "delete")
      .param("id", id));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attribute(
      "message", "Ticket with id = " + id + " is not cancelled"));
    result.andExpect(view().name(VIEW));
  }
}


