package com.epam.jmp.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@ExtendWith(SpringExtension.class)
@SpringJUnitWebConfig
@ContextHierarchy({
  @ContextConfiguration(locations = "classpath:config/daos-test-context.xml"),
  @ContextConfiguration(locations = "classpath:config/services-context.xml"),
  @ContextConfiguration(locations = "classpath:config/application-context.xml")
})
@Sql(scripts = {"/sql/users/users_schema.sql", "/sql/users/import_users.sql"})
class UserControllerTest {
  private static final String PAGE = "0";
  private static final String SIZE = "5";
  private static final String VIEW = "user.html";
  private static final String ERROR_VIEW = "user.html";
  private static final String ATTRIBUTE_USER = "user";

  private MockMvc mockMvc;

  @BeforeEach
  void setup(WebApplicationContext wac) {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
  }

  @Test
  void getUser_email_shouldReturnOk() throws Exception {
    // Given
    final String email = "tony@email.com";

    // When
    final ResultActions result = mockMvc.perform(get("/users")
      .param("email", email));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attributeExists(ATTRIBUTE_USER));
    result.andExpect(view().name(VIEW));
  }

  @Test
  void getEmail_notExistingEmail_shouldReturnNotFound() throws Exception {
    // Given
    final String email = "noemail@email.com";

    // When
    final ResultActions result = mockMvc.perform(get("/users")
      .param("email", email));

    // Then
    result.andDo(print());
    result.andExpect(status().isNotFound());
  }

  @Test
  void getUser_id_shouldReturnOk() throws Exception {
    // Given
    final String id = "1";

    // When
    final ResultActions result = mockMvc.perform(get("/users")
      .param("id", id));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attributeExists(ATTRIBUTE_USER));
    result.andExpect(view().name(VIEW));
  }

  @Test
  void getUser_notExistingId_shouldReturnNotFound() throws Exception {
    // Given
    final String id = "198";

    // When
    final ResultActions result = mockMvc.perform(get("/users")
      .param("id", id));

    // Then
    result.andExpect(status().isNotFound());
  }

  @Test
  void getUsers_name_shouldReturnOk() throws Exception {
    // Given
    final String name = "Tony";

    // When
    final ResultActions result = mockMvc.perform(get("/users")
      .param("name", name)
      .param("page", PAGE)
      .param("size", SIZE));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attributeExists("userPage"));
    result.andExpect(view().name(VIEW));
  }

  @Test
  void getUsers_notExistingName_shouldReturnNotFound() throws Exception {
    // Given
    final String name = "Holly";

    // When
    final ResultActions result = mockMvc.perform(get("/users")
      .param("name", name)
      .param("page", PAGE)
      .param("size", SIZE));

    // Then
    result.andExpect(status().isNotFound());
  }

  @Test
  void insertUser_user_shouldReturnOkAndUser() throws Exception {
    // Given
    final String email = "karol@email.com";
    final String name = "Karol";

    // When
    final ResultActions result = mockMvc.perform(post("/users")
      .param("email", email)
      .param("name", name));

    // Then
    result.andExpect(status().isOk())
      .andExpect(model().attributeExists(ATTRIBUTE_USER))
      .andExpect(view().name(VIEW));
  }

  @Test
  void deleteUser_id_shouldReturnOk() throws Exception {
    // Given
    final String id = "2";

    // When
    final ResultActions result = mockMvc.perform(get("/users")
      .param("_method", "delete")
      .param("id", id));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attribute(
      "message", "User with id = " + id + " is deleted successfully"));
    result.andExpect(view().name(VIEW));
  }

  @Test
  void deleteEvent_notExistingId_shouldReturnOk() throws Exception {
    // Given
    final String id = "16345";

    // When
    final ResultActions result = mockMvc.perform(get("/users")
      .param("_method", "delete")
      .param("id", id));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attribute("message", "User with id = " + id + " is not deleted"));
    result.andExpect(view().name(VIEW));
  }

  @Test
  void updateUser_user_shouldReturnOkAndUser() throws Exception {
    // Given
    final String id = "1";
    final String email = "nelly@email.com";
    final String name = "Nelly";

    // When
    final ResultActions result = mockMvc.perform(get("/users")
      .param("id", id)
      .param("email", email)
      .param("name", name)
      .param("_method", "update"));

    // Then
    result.andExpect(status().isOk())
      .andExpect(model().attributeExists(ATTRIBUTE_USER))
      .andExpect(view().name(VIEW));
  }
}


