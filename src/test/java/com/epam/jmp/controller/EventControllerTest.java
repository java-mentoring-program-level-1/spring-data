package com.epam.jmp.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@ExtendWith(SpringExtension.class)
@SpringJUnitWebConfig
@ContextHierarchy({
  @ContextConfiguration(locations = "classpath:config/daos-test-context.xml"),
  @ContextConfiguration(locations = "classpath:config/services-context.xml"),
  @ContextConfiguration(locations = "classpath:config/application-context.xml")
})
@Sql(scripts = {"/sql/events/events_schema.sql", "/sql/events/import_events.sql"})
class EventControllerTest {
  private static final String PAGE = "0";
  private static final String SIZE = "5";
  private static final String VIEW = "event.html";
  private static final String ERROR_VIEW = "error.html";
  private static final String ATTRIBUTE_EVENT = "event";
  private static final String ATTRIBUTE_EVENT_PAGE = "eventPage";

  private MockMvc mockMvc;

  @BeforeEach
  void setup(WebApplicationContext wac) {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
  }

  @Test
  void getEvents_title_shouldReturnOk() throws Exception {
    // Given
    final String title = "Java-z conference";

    // When
    final ResultActions result = mockMvc.perform(get("/events/")
      .param("title", title)
      .param("page", PAGE)
      .param("size", SIZE));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attributeExists(ATTRIBUTE_EVENT_PAGE));
    result.andExpect(view().name(VIEW));
  }

  @Test
  void getEvents_notExistingTitle_shouldReturnNotFound() throws Exception {
    // Given
    final String title = "cup Of Tea";

    // When
    final ResultActions result = mockMvc.perform(get("/events")
      .param("title", title)
      .param("page", PAGE)
      .param("size", SIZE));

    // Then
    result.andDo(print());
    result.andExpect(status().isNotFound());
    result.andExpect(view().name(ERROR_VIEW));
  }

  @Test
  void getEvents_date_shouldReturnOk() throws Exception {
    // Given
    final String date = "2022-10-15";

    // When
    final ResultActions result = mockMvc.perform(get("/events")
      .param("date", date)
      .param("page", PAGE)
      .param("size", SIZE));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attributeExists(ATTRIBUTE_EVENT_PAGE));
    result.andExpect(view().name(VIEW));
  }

  @Test
  void getEvents_notExistingDate_shouldReturnNotFound() throws Exception {
    // Given
    final String date = "1999-12-20";

    // When
    final ResultActions result = mockMvc.perform(get("/events")
      .param("date", date)
      .param("page", PAGE)
      .param("size", SIZE));

    // Then
    result.andExpect(status().isNotFound());
    result.andExpect(view().name(ERROR_VIEW));
  }

  @Test
  void getEvent_id_shouldReturnOk() throws Exception {
    // Given
    final String id = "1";

    // When
    ResultActions result = mockMvc.perform(get("/events")
      .param("id", id));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attributeExists(ATTRIBUTE_EVENT));
    result.andExpect(view().name(VIEW));
  }

  @Test
  void getEvent_notExistingId_shouldReturnNotFound() throws Exception {
    // Given
    final String id = "156348";

    // When
    final ResultActions result = mockMvc.perform(get("/events")
      .param("id", id));

    // Then
    result.andExpect(status().isNotFound());
    result.andExpect(view().name(ERROR_VIEW));
  }

  @Test
  void deleteEvent_id_shouldReturnOk() throws Exception {
    // Given
    final String id = "2";

    // When
    final ResultActions result = mockMvc.perform(get("/events")
      .param("_method", "delete")
      .param("id", id));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attribute(
      "message", "Event with id = " + id + " is deleted successfully"));
    result.andExpect(view().name(VIEW));
  }

  @Test
  void deleteEvent_notExistingId_shouldReturnOk() throws Exception {
    // Given
    final String id = "16345";

    // When
    final ResultActions result = mockMvc.perform(get("/events")
      .param("_method", "delete")
      .param("id", id));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attribute("message", "Event with id = " + id + " is not deleted"));
    result.andExpect(view().name(VIEW));
  }

  @Test
  void insertEvent_event_shouldReturnOkAndEvent() throws Exception {
    // Given
    final String title = "Good Mood Event";
    final String date = "2022-12-20T06:43";
    final String ticketPrice = "16.5";

    // When
    final ResultActions result = mockMvc.perform(post("/events")
      .param("title", title)
      .param("date", date)
      .param("ticketPrice", ticketPrice));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attributeExists(ATTRIBUTE_EVENT));
    result.andExpect(view().name(VIEW));
  }
}


