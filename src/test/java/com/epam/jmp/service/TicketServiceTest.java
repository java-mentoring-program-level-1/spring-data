package com.epam.jmp.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyCollection;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.jmp.model.event.EventImpl;
import com.epam.jmp.model.exception.EntityNotFoundException;
import com.epam.jmp.model.ticket.Ticket;
import com.epam.jmp.model.ticket.Ticket.Category;
import com.epam.jmp.model.ticket.TicketImpl;
import com.epam.jmp.model.user.UserImpl;
import com.epam.jmp.model.user.account.UserAccountImpl;
import com.epam.jmp.repository.EventRepository;
import com.epam.jmp.repository.TicketRepository;
import com.epam.jmp.repository.UserAccountRepository;
import com.epam.jmp.repository.UserRepository;
import com.epam.jmp.service.impl.EventServiceImpl;
import com.epam.jmp.service.impl.TicketServiceImpl;
import com.epam.jmp.service.impl.UserAccountServiceImpl;
import com.epam.jmp.service.impl.UserServiceImpl;

@ExtendWith(MockitoExtension.class)
class TicketServiceTest {
	@Mock
	TicketRepository ticketRepository;
  @Mock
  UserRepository userRepository;
  @Mock
  EventRepository eventRepository;
	@Mock
  UserAccountRepository accountRepository;
  UserAccountService userAccountService;
  UserServiceImpl userService;
	EventServiceImpl eventService;
	@Mock
	PDFGenerator<TicketImpl> ticketPDFGenerator;
  @Mock
  PriceCalculator priceCalculator;
  TicketServiceImpl ticketService;

  @BeforeEach
	public void init() {
    userAccountService = new UserAccountServiceImpl(accountRepository);
    userService = new UserServiceImpl();
    userService.setAccountService(userAccountService);
    userService.setUserRepository(userRepository);
    eventService = new EventServiceImpl();
    eventService.setRepository(eventRepository);
    ticketService = new TicketServiceImpl(ticketRepository, ticketPDFGenerator, userService, eventService, priceCalculator);
  }

	@Test
	void shouldReturnTicketWhenBookTicket() {
    long userId = 2L;
    long eventId = 3L;
    when(priceCalculator.calculate(any(), any(Category.class))).thenReturn(new BigDecimal("10"));
    when(eventRepository.findById(eventId)).thenReturn(Optional.of(makeEvent(eventId, "event", new Date(), new BigDecimal("15"))));
    when(accountRepository.findByUserId(userId)).thenReturn(Optional.of(makeAccount(1L, userId)));
    when(ticketRepository.save(any())).thenReturn(new TicketImpl());
    Ticket result = ticketService.book(makeTicket(userId, 3L, 15, Category.BAR));

    assertNotNull(result);
    verify(priceCalculator, times(1)).calculate(any(), any(Category.class));
    verify(eventRepository, times(1)).findById(any());
    verify(accountRepository, times(1)).findByUserId(userId);
    verify(ticketRepository, times(1)).save(any());
	}

  @Test
  void shouldGeneratePDFWhenGeneratePDF() {
    when(ticketPDFGenerator.generate(anyList())).thenReturn(new ByteArrayInputStream("PDF".getBytes()));
    ticketService.generatePDFReport(List.of(new TicketImpl()));
    verify(ticketPDFGenerator, times(1)).generate(anyList());
  }

  private EventImpl makeEvent(long id, String title, Date date, BigDecimal price) {
    EventImpl event = new EventImpl();
    event.setId(id);
    event.setTitle(title);
    event.setDate(date);
    event.setTicketPrice(price);
    return event;
  }

  private UserImpl makeUser(long id, String name, String email) {
    UserImpl user = new UserImpl();
    user.setId(id);
    user.setName(name);
    user.setEmail(email);
    return user;
  }

	private TicketImpl makeTicket(long userId, long eventId, int place, Category category) {
		TicketImpl ticket = new TicketImpl();
		ticket.setEvent(makeEvent(eventId, "event", new Date(), new BigDecimal("15.0")));
		ticket.setUser(makeUser(userId, "name", "name@email.com"));
		ticket.setPlace(place);
		ticket.setCategory(category);
		return ticket;
	}

  private UserAccountImpl makeAccount(long id, long userId) {
    UserAccountImpl account = new UserAccountImpl();
    account.setId(id);
    account.setUser(makeUser(userId, "name", "name@email.com"));
    account.setAmount(new BigDecimal("1000"));
    return account;
  }
}
