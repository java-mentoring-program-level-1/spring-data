package com.epam.jmp.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.epam.jmp.model.event.Event;
import com.epam.jmp.model.event.EventImpl;
import com.epam.jmp.model.exception.EntityNotFoundException;
import com.epam.jmp.repository.EventRepository;
import com.epam.jmp.service.impl.EventServiceImpl;

@ExtendWith(MockitoExtension.class)
class EventServiceTest {

	@Mock
	EventRepository repository;
	EventServiceImpl service;
	SimpleDateFormat formatter;

	@BeforeEach
	public void init() {
		service = new EventServiceImpl();
		service.setRepository(repository);
		formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
	}

	@Test
  void shouldReturnNewEventWhenCreateEvent() throws ParseException {
		Event newEvent = new EventImpl();
		Date eventDate = formatter.parse("22-01-2022 01:11:12");
		String eventTitle = "Black event";
		newEvent.setId(1L);
		newEvent.setDate(eventDate);
		newEvent.setTitle(eventTitle);

		when(repository.save(any())).thenReturn(newEvent);

		EventImpl event = new EventImpl();
		event.setDate(eventDate);
		event.setTitle(eventTitle);

		Event result = service.create(event);
		assertEvent(newEvent, result);
	}

	@Test
  void shouldThrowExceptionWhenCreateEventWithEmptyFields() {
		EventImpl event = new EventImpl();
		when(repository.save(any())).thenThrow(InvalidDataAccessApiUsageException.class);
		assertThrows(InvalidDataAccessApiUsageException.class, () -> {
			service.create(event);
		});
	}

	@Test
  void shouldThrowExceptionWhenCreateEventWithInvalidDate() throws ParseException {
		EventImpl event = new EventImpl();
		event.setDate(formatter.parse("01-12-2021 14:00:00")); // date is before today's date
		event.setTitle("old event");
		when(repository.save(event)).thenThrow(DataIntegrityViolationException.class);
		assertThrows(DataIntegrityViolationException.class, () -> {
			service.create(event);
		});
	}

	@Test
  void shouldReturnEventWhenGetEventById() throws ParseException {
		when(repository.findById(any())).thenReturn(Optional.of(new EventImpl()));

		EventImpl result = service.findById(1L);
		assertNotNull(result);
    verify(repository, times(1)).findById(any());
	}

	@Test
  void shouldThrowExceptionWhenGetEventById() {
		when(repository.findById(any())).thenReturn(Optional.empty());
    assertThrows(EntityNotFoundException.class, ()->service.findById(1L));
    verify(repository, times(1)).findById(any());
	}

	@Test
  void shouldReturnUpdatedEventWhenUpdateEvent() throws ParseException {
		EventImpl event = new EventImpl();
    when(repository.save(any())).thenReturn(new EventImpl());
    EventImpl updatedEvent = service.update(event);
    assertNotNull(updatedEvent);
    verify(repository, times(1)).save(any());
  }


	@Test
  void shouldReturnEventsWhenGetEventsByTitle() {
		when(repository.findAllByTitle(any(),any(Pageable.class)))
      .thenReturn(new PageImpl<>(List.of(new EventImpl(), new EventImpl())));
		Page<EventImpl> events = service.findByTitle("event", PageRequest.of(0, 3));
    assertFalse(events.isEmpty());
    verify(repository, times(1)).findAllByTitle(any(), any(Pageable.class));
	}

	@Test
  void shouldReturnEmptyListsWhenGetEventsByTitle() {
		when(repository.findAllByTitle(any(), any(Pageable.class))).thenReturn(Page.empty());
		assertThrows(EntityNotFoundException.class,
      () -> service.findByTitle("event", Pageable.ofSize(3)));
    verify(repository, times(1)).findAllByTitle(any(), any(Pageable.class));
	}

	@Test
  void shouldReturnEventsWhenGetEventsForDay() {
		when(repository.findAllByDateBetween(any(), any(), any(Pageable.class)))
      .thenReturn(new PageImpl<>(List.of(new EventImpl(), new EventImpl())));

    Page<EventImpl> events = service.findForDate(new Date(), Pageable.ofSize(3));
    assertFalse(events.isEmpty());
    verify(repository, times(1)).findAllByDateBetween(any(), any(), any(Pageable.class));
	}

	@Test
	public void shouldThrowExceptionWhenGetEventsForNotExistingDay() {

		when(repository.findAllByDateBetween(any(), any(), any(Pageable.class)))
      .thenReturn(Page.empty());

		assertThrows(EntityNotFoundException.class, ()-> {
			service.findForDate(new Date(), Pageable.ofSize(3));
		});
    verify(repository, atLeastOnce()).findAllByDateBetween(any(), any(), any(Pageable.class));
  }

	private void assertEvent(Event actualEvent, Event expectedEvent) {
		assertEquals(actualEvent.getId(), expectedEvent.getId());
    assertEquals(actualEvent.getDate().getTime(), expectedEvent.getDate().getTime());
		assertEquals(actualEvent.getTitle(), expectedEvent.getTitle());
    assertEquals(actualEvent.getTicketPrice(), expectedEvent.getTicketPrice());
	}
}
