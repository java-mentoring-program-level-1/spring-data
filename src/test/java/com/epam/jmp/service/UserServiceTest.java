package com.epam.jmp.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.epam.jmp.model.exception.EntityNotFoundException;
import com.epam.jmp.model.user.User;
import com.epam.jmp.model.user.UserImpl;
import com.epam.jmp.model.user.account.UserAccountImpl;
import com.epam.jmp.repository.UserRepository;
import com.epam.jmp.service.impl.UserServiceImpl;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
  UserServiceImpl userService;
  @Mock
  UserRepository userRepository;
  @Mock
  UserAccountService userAccountService;

  @BeforeEach
  public void init() {
    userService = new UserServiceImpl();
    userService.setUserRepository(userRepository);
    userService.setAccountService(userAccountService);
  }

  @Test
  void shouldCreateNewUserWhenCreateUser() {
    when(userRepository.save(any())).thenReturn(new UserImpl());
    when(userAccountService.create(any())).thenReturn(new UserAccountImpl());
    UserImpl user = userService.create(new UserImpl());
    assertNotNull(user);
    verify(userRepository, times(1)).save(any());
    verify(userAccountService, times(1)).create(any());
  }

  @Test
  void shouldThrowExceptionWhenCreateUser() {
    UserImpl user = new UserImpl();
    user.setEmail("");
    user.setName("new user");

    when(userRepository.save(user)).thenThrow(DataIntegrityViolationException.class);
    assertThrows(DataIntegrityViolationException.class, () -> userService.create(user));

    verify(userRepository, times(1)).save(user);
  }

  @Test
  void shouldGetUserWhenGetUserById() {
    when(userRepository.findById(any())).thenReturn(Optional.of(new UserImpl()));
    UserImpl user = userService.findById(1L);
    assertNotNull(user);
    verify(userRepository, times(1)).findById(any());
  }

  @Test
  void shouldGetEmptyUserWhenGetUserById() {
    when(userRepository.findById(any())).thenReturn(Optional.empty());
    assertThrows(EntityNotFoundException.class, ()-> userService.findById(0L));
    verify(userRepository, times(1)).findById(any());
  }

  @Test
  void shouldGetUserWhenGetUserByEmail() {
    String email = "newUser@email.com";
    when(userRepository.findByEmail(email)).thenReturn(Optional.of(new UserImpl()));
    UserImpl user = userService.findByEmail(email);
    assertNotNull(user);
    verify(userRepository, times(1)).findByEmail(email);
  }

  @Test
  void shouldReturnEmptyWhenGetUserByEmail() {
    when(userRepository.findByEmail(null)).thenReturn(Optional.empty());
    assertThrows(EntityNotFoundException.class, () -> userService.findByEmail(null));
    verify(userRepository, times(1)).findByEmail(null);
  }

  @Test
  void shouldReturnUpdatedUserWhenUpdateUser() {
    UserImpl user = new UserImpl();
    when(userRepository.save(user)).thenReturn(user);
    User updatedUser = userService.update(user);
    assertNotNull(updatedUser);
    verify(userRepository, times(1)).save(user);
  }

  @Test
  void shouldReturnExceptionWhenUpdateUserWithInvalidEmail() {
    when(userRepository.save(any(UserImpl.class)))
      .thenThrow(DataIntegrityViolationException.class);
    assertThrows(DataIntegrityViolationException.class, () -> {
      userService.update(new UserImpl());
    });
    verify(userRepository, times(1)).save(any(UserImpl.class));
  }

  @Test
  void shouldReturnExceptionWhenUpdateUserWithInvalidUser() {
    when(userRepository.save(null)).thenThrow(InvalidDataAccessApiUsageException.class);
    assertThrows(InvalidDataAccessApiUsageException.class, () -> userService.update(null));
    verify(userRepository, times(1)).save(null);
  }

  @Test
  void shouldReturnUsersWhenGetUsersByName() {
    when(userRepository.findAllByName(any(), any(Pageable.class)))
      .thenReturn(new PageImpl<>(List.of(new UserImpl(), new UserImpl())));

    Page<UserImpl> users = userService.findByName("name", Pageable.ofSize(3));
    assertFalse(users.isEmpty());
    verify(userRepository, times(1)).findAllByName(any(), any(Pageable.class));
  }

  @Test
  void shouldReturnEmptyListWhenGetUsersByName() {
    when(userRepository.findAllByName(any(), any(Pageable.class))).thenReturn(Page.empty());
    Page<UserImpl> users = userService.findByName("name", Pageable.ofSize(3));
    assertTrue(users.isEmpty());
    verify(userRepository, atLeastOnce()).findAllByName(any(), any(Pageable.class));
  }
}
