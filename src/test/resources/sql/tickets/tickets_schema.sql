DROP TABLE IF EXISTS public."TICKET" CASCADE;

CREATE TABLE IF NOT EXISTS public."TICKET"
(
    id bigint NOT NULL,
    category character varying(255) COLLATE pg_catalog."default",
    place integer NOT NULL,
    eventid bigint,
    userid bigint,
    CONSTRAINT "TICKET_pkey" PRIMARY KEY (id),
    CONSTRAINT uk1ltpu0s3ujun1sea6hwt8k90m UNIQUE (userid, eventid),
    CONSTRAINT ukj5jjstq8bqw2vmjpqakvu8hwd UNIQUE (eventid, place, category),
    CONSTRAINT fklh1d8tyou4knk2qkjl580eo01 FOREIGN KEY (userid)
        REFERENCES public."USER" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

DROP SEQUENCE IF EXISTS public."ticket-sequence" CASCADE;

CREATE SEQUENCE IF NOT EXISTS public."ticket-sequence" INCREMENT BY 1 START WITH 1 OWNED BY public."TICKET".id;