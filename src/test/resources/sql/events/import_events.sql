INSERT INTO public."EVENT"(id, date, ticketprice, title) VALUES (nextval('event-sequence'), '2022-10-15 13:30', 15.0, 'Java-z conference');
INSERT INTO public."EVENT"(id, date, ticketprice, title) VALUES (nextval('event-sequence'), '2022-10-16 13:30', 19.5, 'C#-z conference');
INSERT INTO public."EVENT"(id, date, ticketprice, title) VALUES (nextval('event-sequence'), '2022-06-15 13:30', 20.0, 'Python-z conference');
INSERT INTO public."EVENT"(id, date, ticketprice, title) VALUES (nextval('event-sequence'), '2022-10-15 13:30', 13.0, 'C++-z conference');
INSERT INTO public."EVENT"(id, date, ticketprice, title) VALUES (nextval('event-sequence'), '2022-10-15 15:30', 10.0, 'C-z conference');
