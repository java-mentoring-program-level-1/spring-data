package com.epam.jmp.facade.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.epam.jmp.controller.assembler.EventListResourceAssembler;
import com.epam.jmp.controller.assembler.TicketListResourceAssembler;
import com.epam.jmp.controller.assembler.UserListResourceAssembler;
import com.epam.jmp.facade.BookingFacade;
import com.epam.jmp.model.event.Event;
import com.epam.jmp.model.event.EventContainer;
import com.epam.jmp.model.event.EventImpl;
import com.epam.jmp.model.event.EventListResource;
import com.epam.jmp.model.event.EventResource;
import com.epam.jmp.model.ticket.Ticket;
import com.epam.jmp.model.ticket.TicketContainer;
import com.epam.jmp.model.ticket.TicketImpl;
import com.epam.jmp.model.ticket.TicketListResource;
import com.epam.jmp.model.ticket.TicketResource;
import com.epam.jmp.model.user.User;
import com.epam.jmp.model.user.UserContainer;
import com.epam.jmp.model.user.UserImpl;
import com.epam.jmp.model.user.UserListResource;
import com.epam.jmp.model.user.UserResource;
import com.epam.jmp.model.user.account.UserAccount;
import com.epam.jmp.model.user.account.UserAccountImpl;
import com.epam.jmp.service.EventService;
import com.epam.jmp.service.TicketService;
import com.epam.jmp.service.UserService;
import com.epam.jmp.service.impl.EventOXMapper;
import com.epam.jmp.service.impl.TicketOXMapper;
import com.epam.jmp.service.impl.UserOXMapper;

public class BookingFacadeImpl implements BookingFacade {

  private UserService userService;
  private EventService eventService;
  private TicketService ticketService;
  private EventListResourceAssembler eventResourcesAssembler;
  private UserListResourceAssembler userResourcesAssembler;
  private TicketListResourceAssembler ticketResourcesAssembler;
  private UserOXMapper userOXMapper;
  private EventOXMapper eventOXMapper;
  private TicketOXMapper ticketOXMapper;

  public BookingFacadeImpl(UserService userService,
    EventService eventService, TicketService ticketService,
    EventListResourceAssembler eventResourcesAssembler,
    UserListResourceAssembler userResourcesAssembler,
    TicketListResourceAssembler ticketResourcesAssembler,
    UserOXMapper oxMapper, EventOXMapper eventOXMapper,
    TicketOXMapper ticketOXMapper) {

    this.userService = userService;
    this.eventService = eventService;
    this.ticketService = ticketService;
    this.eventResourcesAssembler = eventResourcesAssembler;
    this.userResourcesAssembler = userResourcesAssembler;
    this.ticketResourcesAssembler = ticketResourcesAssembler;
    this.userOXMapper = oxMapper;
    this.eventOXMapper = eventOXMapper;
    this.ticketOXMapper = ticketOXMapper;
  }

  @Override
  public EventResource getEventById(long eventId) {
    return eventResourcesAssembler.toResource(eventService.findById(eventId));
  }

  @Override
  public EventListResource getEventsByTitle(String title, Pageable pageable) {
    Page<EventImpl> eventPage = eventService.findByTitle(title, pageable);
    return eventResourcesAssembler.toResources(eventPage);
  }

  @Override
  public EventListResource getEventsForDay(Date day, Pageable pageable) {
    Page<EventImpl> eventPage = eventService.findForDate(day, pageable);
    return eventResourcesAssembler.toResources(eventPage);
  }

  @Override
  public EventResource createEvent(EventResource eventResource) {
    Event result = eventService.create(eventResourcesAssembler.toEntity(eventResource));
    return eventResourcesAssembler.toResource(result);
  }

  @Override
  public EventResource updateEvent(EventResource eventResource) {
    Event result = eventService.update(eventResourcesAssembler.toEntity(eventResource));
    return eventResourcesAssembler.toResource(result);
  }

  @Override
  public boolean deleteEvent(long eventId) {
    return eventService.deleteById(eventId);
  }

  @Override
  public UserResource getUserById(long userId) {
    return userResourcesAssembler.toResource(userService.findById(userId));
  }

  @Override
  public UserResource getUserByEmail(String email) {
    return userResourcesAssembler.toResource(userService.findByEmail(email));
  }

  @Override
  public UserListResource getUsersByName(String name, Pageable pageable) {
    Page<UserImpl> userPage = userService.findByName(name, pageable);
    return userResourcesAssembler.toResources(userPage);
  }

  @Override
  public UserResource createUser(UserResource userResource) {
    User result = userService.create(userResourcesAssembler.toEntity(userResource));

    UserAccount userAccount = new UserAccountImpl();
    userAccount.setUser(result);
    return userResourcesAssembler.toResource(result);
  }

  @Override
  public UserResource updateUser(UserResource userResource) {
    User result = userService.update(userResourcesAssembler.toEntity(userResource));
    return userResourcesAssembler.toResource(result);
  }

  @Override
  public boolean deleteUser(long userId) {
    return userService.deleteById(userId);
  }

  @Override
  public TicketResource bookTicket(TicketResource ticketResource) {
    Ticket result = ticketService.book(ticketResourcesAssembler.toEntity(ticketResource));
    return ticketResourcesAssembler.toResource(result);
  }

  @Override
  public TicketListResource getBookedTickets(UserResource userResource, Pageable pageable) {
    Page<TicketImpl> ticketPage = ticketService.findByUser(
      userResourcesAssembler.toEntity(userResource), pageable);
    return ticketResourcesAssembler.toResources(ticketPage);
  }

  @Override
  public TicketListResource getBookedTickets(EventResource eventResource, Pageable pageable) {
    Page<TicketImpl> ticketPage = ticketService.findByEvent(
      eventResourcesAssembler.toEntity(eventResource), pageable);
    return ticketResourcesAssembler.toResources(ticketPage);
  }

  @Override
  public boolean cancelTicket(long ticketId) {
    return ticketService.cancel(ticketId);
  }


  @Override
  public ByteArrayInputStream ticketReport(TicketListResource ticketListResource) {
    List<TicketImpl> ticketList = ticketListResource.getTickets().stream()
      .map(ticketResourcesAssembler::toEntity)
      .collect(Collectors.toList());

    return ticketService.generatePDFReport(ticketList);
  }

  @Override
  public void preloadTickets() throws IOException {
    UserContainer userContainer = userOXMapper.xmlToObject();
    userContainer.getUsers().forEach(this::createUser);
    EventContainer eventContainer = eventOXMapper.xmlToObject();
    eventContainer.getEvents().forEach(this::createEvent);
    TicketContainer ticketContainer = ticketOXMapper.xmlToObject();
    ticketContainer.getTickets().forEach(this::bookTicket);
  }

}
