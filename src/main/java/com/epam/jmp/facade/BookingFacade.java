package com.epam.jmp.facade;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;

import com.epam.jmp.model.event.EventListResource;
import com.epam.jmp.model.event.EventResource;
import com.epam.jmp.model.ticket.Ticket;
import com.epam.jmp.model.ticket.TicketListResource;
import com.epam.jmp.model.ticket.TicketResource;
import com.epam.jmp.model.user.UserListResource;
import com.epam.jmp.model.user.UserResource;

/**
 * Groups together all operations related to tickets booking.
 * Created by maksym_govorischev.
 */
public interface BookingFacade {

    /**
     * Gets event by its id.
     * @return Event.
     */
    EventResource getEventById(long eventId);

    /**
     * Get list of events by matching title. Title is matched using 'contains' approach.
     * In case nothing was found, empty list is returned.
     * @param title Event title or it's part.
     * @param pageable Pagination param, includes page number and page size
     * @return Page of events.
     */
    EventListResource getEventsByTitle(String title, Pageable pageable);

    /**
     * Get list of events for specified day.
     * In case nothing was found, empty list is returned.
     * @param day Date object from which day information is extracted.
     * @param pageable Pagination param, includes page number and page size
     * @return Page of events.
     */
    EventListResource getEventsForDay(Date day, Pageable pageable);

    /**
     * Creates new event. Event id should be auto-generated.
     * @param eventResource Event data.
     * @return Created Event object.
     */
    EventResource createEvent(EventResource eventResource);

    /**
     * Updates event using given data.
     * @param eventResource Event data for update. Should have id set.
     * @return Updated Event object.
     */
    EventResource updateEvent(EventResource eventResource);

    /**
     * Deletes event by its id.
     * @param eventId Event id.
     * @return Flag that shows whether event has been deleted.
     */
    boolean deleteEvent(long eventId);

    /**
     * Gets user by its id.
     * @return User.
     */
    UserResource getUserById(long userId);

    /**
     * Gets user by its email. Email is strictly matched.
     * @return User.
     */
    UserResource getUserByEmail(String email);

    /**
     * Get list of users by matching name. Name is matched using 'contains' approach.
     * In case nothing was found, empty list is returned.
     * @param name Users name or it's part.
     * @param pageable Pagination param, includes page number and page size
     * @return Page of users.
     */
    UserListResource getUsersByName(String name, Pageable pageable);

    /**
     * Creates new user. User id should be auto-generated.
     * @param userResource User data.
     * @return Created User object.
     */
    UserResource createUser(UserResource userResource);

    /**
     * Updates user using given data.
     * @param userResource User data for update. Should have id set.
     * @return Updated User object.
     */
    UserResource updateUser(UserResource userResource);

    /**
     * Deletes user by its id.
     * @param userId User id.
     * @return Flag that shows whether user has been deleted.
     */
    boolean deleteUser(long userId);

    /**
     * Book ticket for a specified event on behalf of specified user.
     * @param ticket Ticket
     * @return Booked ticket object.
     * @throws java.lang.IllegalStateException if this place has already been booked.
     */
    TicketResource bookTicket(TicketResource ticketResource);

    /**
     * Get all booked tickets for specified user. Tickets should be sorted by event date in descending order.
     * @param userResource User data
     * @param pageable Pagination param, includes page number and page size
     * @return List of Ticket objects.
     */
    TicketListResource getBookedTickets(UserResource userResource, Pageable pageable);

    /**
     * Get all booked tickets for specified event. Tickets should be sorted in by user email in ascending order.
     * @param eventResource Event data
     * @param pageable Pagination param, includes page number and page size
     * @return List of Ticket objects.
     */
    TicketListResource getBookedTickets(EventResource eventResource, Pageable pageable);

    /**
     * Cancel ticket with a specified id.
     * @param ticketId Ticket id.
     * @return Flag whether anything has been canceled.
     */
    boolean cancelTicket(long ticketId);

    /**
     * Generate a pdf report for the booked tickets of a user.
     * @param ticketListResource TicketListResource
     * @return ByteArrayInputStream
     */
    ByteArrayInputStream ticketReport(TicketListResource ticketListResource);

    /**
     * Unmarshall ticket objects from xml
     */
    void preloadTickets() throws IOException;
}
