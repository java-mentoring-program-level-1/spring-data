package com.epam.jmp.converter;

import static com.epam.jmp.model.ticket.Ticket.Category.*;

import org.springframework.core.convert.converter.Converter;

import com.epam.jmp.model.ticket.Ticket.Category;

public class StringToCategoryConverter implements Converter<String, Category> {
  @Override
  public Category convert(String source) {
    switch (source) {
      case "Premium": return PREMIUM;
      case "Bar": return BAR;
      default: return STANDARD;
    }
  }
}
