package com.epam.jmp.model.user;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder = {"id", "name", "email"})
public class UserResource {
  private long id;
  private String name;
  private String email;

  public UserResource() {}

  public UserResource(long id, String name, String email) {
    this.id = id;
    this.name = name;
    this.email = email;
  }

  @XmlAttribute(name = "id")
  public long getId() {
    return id;
  }

  @XmlAttribute(name = "name")
  public String getName() {
    return name;
  }

  @XmlAttribute(name = "email")
  public String getEmail() {
    return email;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}
