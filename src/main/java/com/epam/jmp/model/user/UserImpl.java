package com.epam.jmp.model.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "\"USER\"")
public class UserImpl implements User, Serializable {
	@Id
	@GeneratedValue(generator = "\"user-sequence\"", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "\"user-sequence\"", allocationSize = 1)
	private long id;
	private String name;

	@Column(unique = true, nullable = false)
	private String email;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
