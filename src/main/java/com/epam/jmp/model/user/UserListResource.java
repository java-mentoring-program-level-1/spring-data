package com.epam.jmp.model.user;

import java.util.Collection;

import com.epam.jmp.model.AbstractListResource;

public class UserListResource extends AbstractListResource {
  private final Collection<UserResource> users;

  public UserListResource(Collection<UserResource> users, int pageNumber, int pageSize, int totalPages, long totalElements) {
    super(pageNumber, pageSize, totalPages, totalElements);
    this.users = users;
  }

  public Collection<UserResource> getUsers() {
    return users;
  }
}
