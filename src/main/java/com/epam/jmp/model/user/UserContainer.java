package com.epam.jmp.model.user;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "users")
public class UserContainer {
  private List<UserResource> users;

  @XmlElement(name = "user")
  public List<UserResource> getUsers() {
    return users;
  }

  public void setUsers(List<UserResource> users) {
    this.users = users;
  }
}
