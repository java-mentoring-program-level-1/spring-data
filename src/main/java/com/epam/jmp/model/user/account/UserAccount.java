package com.epam.jmp.model.user.account;

import java.math.BigDecimal;

import com.epam.jmp.model.user.User;

public interface UserAccount {
  long getId();
  void setId(long id);
  User getUser();
  void setUser(User user);
  BigDecimal getAmount();
  void setAmount(BigDecimal amount);

}
