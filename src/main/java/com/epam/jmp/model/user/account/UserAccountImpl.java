package com.epam.jmp.model.user.account;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.epam.jmp.model.user.User;
import com.epam.jmp.model.user.UserImpl;

@Entity
@Table(name = "\"USER_ACCOUNT\"")
public class UserAccountImpl implements UserAccount, Serializable {
  @Id
  @GeneratedValue(generator = "\"user-account-sequence\"", strategy = GenerationType.SEQUENCE)
  @SequenceGenerator(name = "\"user-account-sequence\"", allocationSize = 1)
  private long id;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "userId", nullable = false)
  private UserImpl user;

  private BigDecimal amount = new BigDecimal("1000");

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = (UserImpl) user;
  }
}
