package com.epam.jmp.model.event;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "events")
public class EventContainer {
  private List<EventResource> events;

  @XmlElement(name = "event")
  public List<EventResource> getEvents() {
    return events;
  }

  public void setEvents(List<EventResource> events) {
    this.events = events;
  }
}
