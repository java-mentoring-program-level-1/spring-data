package com.epam.jmp.model.event;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.epam.jmp.model.event.Event;

@Entity
@Table(name = "\"EVENT\"")
public class EventImpl implements Event, Serializable {
  @Id
  @GeneratedValue(generator = "\"event-sequence\"", strategy = GenerationType.SEQUENCE)
  @SequenceGenerator(name = "\"event-sequence\"", allocationSize = 1)
  private long id;

  @Column(unique = true, nullable = false)
  private String title;

  @Column(nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date date;

  private BigDecimal ticketPrice;

  public BigDecimal getTicketPrice() {
    return ticketPrice;
  }

  public void setTicketPrice(BigDecimal ticketPrice) {
    this.ticketPrice = ticketPrice;
  }

  @Override
  public long getId() {
    return this.id;
  }

  @Override
  public void setId(long id) {
    this.id = id;
  }

  @Override
  public String getTitle() {
    return this.title;
  }

  @Override
  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public Date getDate() {
    return this.date;
  }

  @Override
  public void setDate(Date date) {
    this.date = date;
  }
}
