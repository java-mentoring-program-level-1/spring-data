package com.epam.jmp.model.event;

import java.util.Collection;

import com.epam.jmp.model.AbstractListResource;

public class EventListResource extends AbstractListResource {
  private final Collection<EventResource> events;

  public EventListResource(Collection<EventResource> events, int pageNumber, int pageSize, int totalPages, long totalElements) {
    super(pageNumber, pageSize, totalPages, totalElements);
    this.events = events;
  }

  public Collection<EventResource> getEvents() {
    return events;
  }
}
