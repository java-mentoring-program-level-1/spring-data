package com.epam.jmp.model.event;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder = {"id", "title", "date", "ticketPrice"})
public class EventResource {
  private long id;
  private String title;
  private Date date;
  private BigDecimal ticketPrice;

  public EventResource() {}

  public EventResource(long id, String title, Date date, BigDecimal ticketPrice) {
    this.id = id;
    this.title = title;
    this.date = date;
    this.ticketPrice = ticketPrice;
  }

  @XmlAttribute(name = "id")
  public long getId() {
    return id;
  }

  @XmlAttribute(name = "title")
  public String getTitle() {
    return title;
  }

  @XmlAttribute(name = "date")
  public Date getDate() {
    return date;
  }

  @XmlAttribute(name = "ticketPrice")
  public BigDecimal getTicketPrice() {
    return ticketPrice;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public void setTicketPrice(BigDecimal ticketPrice) {
    this.ticketPrice = ticketPrice;
  }
}
