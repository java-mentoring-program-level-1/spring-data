package com.epam.jmp.model.ticket;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.epam.jmp.model.ticket.TicketImpl;

@XmlRootElement(name = "tickets")
public class TicketContainer {
  private List<TicketResource> tickets;

  @XmlElement(name = "ticket")
  public List<TicketResource> getTickets() {
    return tickets;
  }

  public void setTickets(List<TicketResource> tickets) {
    this.tickets = tickets;
  }
}
