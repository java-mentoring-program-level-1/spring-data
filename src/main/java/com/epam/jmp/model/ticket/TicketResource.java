package com.epam.jmp.model.ticket;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.epam.jmp.model.ticket.Ticket.Category;

@XmlRootElement
@XmlType(propOrder = {"userId", "eventId", "category", "place"})
public class TicketResource {
  private long id;
  private long userId;
  private long eventId;
  private Category category;
  private int place;

  public TicketResource() {}

  public TicketResource(long id, long userId, long eventId, Category category, int place) {
    this.id = id;
    this.userId = userId;
    this.eventId = eventId;
    this.category = category;
    this.place = place;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  public void setEventId(long eventId) {
    this.eventId = eventId;
  }

  public void setCategory(Category category) {
    this.category = category;
  }

  public void setPlace(int place) {
    this.place = place;
  }

  @XmlTransient
  public long getId() {
    return id;
  }

  @XmlAttribute(name = "userId")
  public long getUserId() {
    return userId;
  }

  @XmlAttribute(name = "eventId")
  public long getEventId() {
    return eventId;
  }

  @XmlAttribute(name = "category")
  public Category getCategory() {
    return category;
  }

  @XmlAttribute(name = "place")
  public int getPlace() {
    return place;
  }
}
