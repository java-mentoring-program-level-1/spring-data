package com.epam.jmp.model.ticket;

import java.util.Collection;

import com.epam.jmp.model.AbstractListResource;

public class TicketListResource extends AbstractListResource {
  private final Collection<TicketResource> tickets;

  public TicketListResource(Collection<TicketResource> tickets, int pageNumber, int pageSize, int totalPages, long totalElements) {
    super(pageNumber, pageSize, totalPages, totalElements);
    this.tickets = tickets;
  }

  public Collection<TicketResource> getTickets() {
    return tickets;
  }
}
