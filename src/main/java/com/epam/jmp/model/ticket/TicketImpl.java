package com.epam.jmp.model.ticket;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.epam.jmp.model.event.Event;
import com.epam.jmp.model.event.EventImpl;
import com.epam.jmp.model.user.User;
import com.epam.jmp.model.user.UserImpl;

@Entity
@Table(name = "\"TICKET\"",
  uniqueConstraints = {
  @UniqueConstraint(columnNames = {"userId", "eventId"}),
  @UniqueConstraint(columnNames = {"eventId", "place", "category"})})
public class TicketImpl implements Ticket, Serializable {

  @Id
  @GeneratedValue(generator = "\"ticket-sequence\"", strategy = GenerationType.SEQUENCE)
  @SequenceGenerator(name = "\"ticket-sequence\"", allocationSize = 1)
  private long id;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "eventId")
  private EventImpl event;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "userId")
  private UserImpl user;

  @Enumerated(EnumType.STRING)
  private Category category = Category.STANDARD; // default value

  @Column(nullable = false)
  private int place;

  @Override
  public long getId() {
    return this.id;
  }

  @Override
  public void setId(long id) {
    this.id = id;
  }

  @Override
  public EventImpl getEvent() {
    return event;
  }

  @Override
  public void setEvent(Event event) {
    this.event = (EventImpl) event;
  }

  @Override
  public UserImpl getUser() {
    return user;
  }

  @Override
  public void setUser(User user) {
    this.user = (UserImpl) user;
  }

  @Override
  public Category getCategory() {
    return this.category;
  }

  @Override
  public void setCategory(Category category) {
    this.category = category;
  }

  @Override
  public int getPlace() {
    return this.place;
  }

  @Override
  public void setPlace(int place) {
    this.place = place;
  }

}
