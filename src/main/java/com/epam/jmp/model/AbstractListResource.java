package com.epam.jmp.model;

public abstract class AbstractListResource {
  private final int pageNumber;
  private final int pageSize;
  private final int totalPages;
  private final long totalElements;

  protected AbstractListResource(int pageNumber, int pageSize, int totalPages, long totalElements) {
    this.pageNumber = pageNumber;
    this.pageSize = pageSize;
    this.totalPages = totalPages;
    this.totalElements = totalElements;
  }

  public int getPageNumber() {
    return pageNumber;
  }

  public int getPageSize() {
    return pageSize;
  }

  public int getTotalPages() {
    return totalPages;
  }

  public long getTotalElements() {
    return totalElements;
  }
}
