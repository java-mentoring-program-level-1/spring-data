package com.epam.jmp.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.jmp.facade.BookingFacade;
import com.epam.jmp.model.user.User;
import com.epam.jmp.model.user.UserImpl;
import com.epam.jmp.model.user.UserListResource;
import com.epam.jmp.model.user.UserResource;

@Controller
public class UserController {
	@Autowired
	private BookingFacade facade;
	private static final String VIEW = "user.html";
	private static Logger log = LoggerFactory.getLogger(UserController.class);
  private static final String ATTRIBUTE_USER = "user";


	@GetMapping(value = "/users", params = "email")
	public ModelAndView getUserByEmail(@RequestParam("email") String email, ModelAndView modelAndView) {
		log.info("get user by email");
		modelAndView.setViewName(VIEW);

		UserResource user = facade.getUserByEmail(email);
    modelAndView.addObject(ATTRIBUTE_USER, user);

		return modelAndView;
	}

	@GetMapping(value = "/users", params = "id")
	public ModelAndView getUserById(@RequestParam("id") long id, ModelAndView modelAndView) {
		log.info("get user by id");
		modelAndView.setViewName(VIEW);

		UserResource user = facade.getUserById(id);
		modelAndView.addObject(ATTRIBUTE_USER, user);

		return modelAndView;
	}

	@GetMapping(value = "/users", params={"name", "page", "size"})
	public ModelAndView getUsersByName(@RequestParam("name") String name,
		Pageable pageable, ModelAndView modelAndView) {
		log.info("get users by name");
		modelAndView.setViewName(VIEW);

		UserListResource users = facade.getUsersByName(name, pageable);
		modelAndView.addObject("userPage", users);

		return modelAndView;
	}

	@PostMapping(value = "users", params = {"name", "email"})
	public ModelAndView createUser(@RequestParam("name") String name,
		@RequestParam("email") String email, ModelAndView modelAndView) {
		log.info("create user");
		modelAndView.setViewName(VIEW);

		UserResource user = new UserResource(0, name, email);
		UserResource result = facade.createUser(user);
		modelAndView.addObject(ATTRIBUTE_USER, result);

		return modelAndView;
	}

	@GetMapping(value = "/users", params = {"_method", "id"})
	public ModelAndView deleteUser(@RequestParam("_method") String method,
		@RequestParam("id") long id, ModelAndView modelAndView) {
		log.info("deleting user");
		modelAndView.setViewName(VIEW);

		if("delete".equals(method)) {
			boolean result = facade.deleteUser(id);
			String message = "User with id = " + id + " is ";
			message += result ? "deleted successfully" : "not deleted";

			modelAndView.addObject("message", message);
		}
		return modelAndView;
	}

	@GetMapping(value = "/users", params = {"_method", "id", "name", "email"})
	public ModelAndView updateUser(@RequestParam("_method") String method,
		@RequestParam("id") long id,
		@RequestParam(value = "name", required = false) String name,
		@RequestParam(value = "email", required = false) String email,
		ModelAndView modelAndView) {
		log.info("updating user");
		modelAndView.setViewName(VIEW);

		if("update".equals(method)) {
			UserResource user = new UserResource(id, name, email);
			UserResource result = facade.updateUser(user);
			modelAndView.addObject(ATTRIBUTE_USER, result);
		}

		return modelAndView;
	}
}
