package com.epam.jmp.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.jmp.facade.BookingFacade;
import com.epam.jmp.model.event.EventResource;
import com.epam.jmp.model.ticket.Ticket;
import com.epam.jmp.model.ticket.Ticket.Category;
import com.epam.jmp.model.ticket.TicketListResource;
import com.epam.jmp.model.ticket.TicketResource;
import com.epam.jmp.model.user.UserResource;

@Controller
public class TicketController {
	Logger log = LoggerFactory.getLogger(TicketController.class);
	private static final String VIEW = "ticket.html";
  private static final String ATTRIBUTE_TICKET = "ticket";
  private static final String ATTRIBUTE_TICKET_PAGE = "ticketPage";

	@Autowired
	private BookingFacade facade;

	@GetMapping(value = "/tickets", params = {"userId", "page", "size"})
	public ModelAndView getTicketsByUser(
		@RequestParam(value = "userId", required = false ) long userId,
		Pageable pageable, ModelAndView modelAndView) {

    log.info("get booked tickets by user");
		modelAndView.setViewName(VIEW);

    UserResource user = new UserResource(userId, null, null);
		TicketListResource bookedTickets = facade.getBookedTickets(user, pageable);
		modelAndView.addObject(ATTRIBUTE_TICKET_PAGE, bookedTickets);

		return modelAndView;
	}

	@GetMapping(value = "/tickets", params = {"userId", "page", "size"},
    produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<InputStreamResource> getTicketsReport(
		@RequestParam(value = "userId", required = false ) long userId, Pageable pageable) {

    log.info("get booked tickets report");

    UserResource user = new UserResource(userId, null, null);
		TicketListResource bookedTickets = facade.getBookedTickets(user, pageable);
		ByteArrayInputStream inputStream = facade.ticketReport(bookedTickets);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "inline; filename=ticket-report.pdf");

		return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(inputStream));
	}

	@GetMapping(value = "/tickets", params = {"eventId", "page", "size"})
	public ModelAndView getTicketsByEventId(@RequestParam("eventId") long eventId,
		Pageable pageable, ModelAndView modelAndView) {

		log.info("get booked tickets by event");
		modelAndView.setViewName(VIEW);
		EventResource event = new EventResource(eventId, null, null, null);
		TicketListResource bookedTickets = facade.getBookedTickets(event, pageable);
		modelAndView.addObject(ATTRIBUTE_TICKET_PAGE, bookedTickets);
		return modelAndView;
	}

	@PostMapping(value = "/tickets", params = {"userId", "eventId", "place", "category"})
	public ModelAndView bookTicket(@RequestParam("userId") long userId,
		@RequestParam("eventId") long eventId, @RequestParam("place") int place,
		@RequestParam(value = "category" , required = false) Category category,
		ModelAndView modelAndView) {

		log.info("book a ticket");
		modelAndView.setViewName(VIEW);

		TicketResource ticketResource = new TicketResource(0, userId, eventId, category, place);
		TicketResource ticket = facade.bookTicket(ticketResource);
		String message = "Ticket booked successfully";
		modelAndView.addObject("message", message);
		modelAndView.addObject(ATTRIBUTE_TICKET, ticket);
		return modelAndView;
	}

	@GetMapping(value = "/tickets", params = {"_method", "id"})
	public ModelAndView cancelTicket(@RequestParam("_method") String method,
		@RequestParam("id") long id, ModelAndView modelAndView) {

		log.info("cancelling ticket");
		modelAndView.setViewName(VIEW);

		if("delete".equals(method)) {
			boolean result = facade.cancelTicket(id);
			String message = "Ticket with id = " + id + " is ";
			message += result ? "cancelled successfully" : "not cancelled";

			modelAndView.addObject("message", message);
		}
		return modelAndView;
	}

	@GetMapping("/tickets/load")
	public ModelAndView preloadTickets(ModelAndView modelAndView) throws IOException {
		log.info("preloading tickets");
		modelAndView.setViewName(VIEW);

		facade.preloadTickets();
		modelAndView.addObject("message", "preloaded successfully");

		return modelAndView;
	}
}
