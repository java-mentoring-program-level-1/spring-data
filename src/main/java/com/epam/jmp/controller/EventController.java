package com.epam.jmp.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.jmp.facade.BookingFacade;
import com.epam.jmp.model.event.EventListResource;
import com.epam.jmp.model.event.EventResource;

@Controller
@PropertySource("classpath:application.properties")
public class EventController {
  private static final Logger log = LoggerFactory.getLogger(EventController.class);
  private static final String VIEW = "event.html";
  private static final String ATTRIBUTE_EVENT = "event";
  private static final String ATTRIBUTE_EVENT_PAGE = "eventPage";

  @Autowired
  private BookingFacade facade;
  @Autowired
  private SimpleDateFormat formatter;

  @GetMapping(value = "/events", params = {"title", "page", "size"})
  public ModelAndView getEventsByTitle(@RequestParam("title") String title,
    Pageable pageable, ModelAndView modelAndView) {

    log.info("get events by title");
    modelAndView.setViewName(VIEW);

    EventListResource events = facade.getEventsByTitle(title, pageable);
    modelAndView.addObject(ATTRIBUTE_EVENT_PAGE, events);

    return modelAndView;
  }

  @GetMapping(value = "/events", params = {"date", "page", "size"})
  public ModelAndView getEventsForDate(@RequestParam("date")
    @DateTimeFormat(pattern = "yyyy-MM-dd") Date date, Pageable pageable,
    ModelAndView modelAndView) {

    log.info("get events for date");
    modelAndView.setViewName(VIEW);
    formatter.applyPattern("dd-MM-yyyy");
    EventListResource events = facade.getEventsForDay(date, pageable);
    modelAndView.addObject(ATTRIBUTE_EVENT_PAGE, events);
    return modelAndView;
  }

  @GetMapping(value = "/events", params = "id")
  public ModelAndView getEventById(@RequestParam("id") long id, ModelAndView modelAndView) {
    log.info("get event by id");
    modelAndView.setViewName(VIEW);
    EventResource event = facade.getEventById(id);
    modelAndView.addObject(ATTRIBUTE_EVENT, event);
    return modelAndView;
  }

  @GetMapping(value = "/events", params = {"_method", "id"})
  public ModelAndView deleteEvent(@RequestParam("_method") String method,
    @RequestParam("id") long id, ModelAndView modelAndView) {

    log.info("deleting event");
    modelAndView.setViewName(VIEW);

    if ("delete".equals(method)) {
      boolean result = facade.deleteEvent(id);
      String message = "Event with id = " + id + " is ";
      message += result ? "deleted successfully" : "not deleted";

      modelAndView.addObject("message", message);
    }
    return modelAndView;
  }

  @PostMapping(value = "/events", params = {"title", "date", "ticketPrice"})
  public ModelAndView insertEvent(@RequestParam("title") String title,
    @RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm") Date date,
    @RequestParam("ticketPrice") BigDecimal ticketPrice, ModelAndView modelAndView) {

    log.info("inserting event");
    modelAndView.setViewName(VIEW);

    EventResource eventResource = new EventResource(0, title, date, ticketPrice);

    EventResource result = facade.createEvent(eventResource);
    modelAndView.addObject(ATTRIBUTE_EVENT, result);

    return modelAndView;
  }

  @GetMapping(value = "/events", params = {"_method", "id", "title", "date", "ticketPrice"})
  public ModelAndView updateUser(@RequestParam("_method") String method,
    @RequestParam("id") long id, @RequestParam(value = "title", required = false) String title,
    @RequestParam(value = "date", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
      Date date, @RequestParam(value = "ticketPrice", required = false) BigDecimal ticketPrice,
    ModelAndView modelAndView) {

    log.info("updating event");
    modelAndView.setViewName(VIEW);

    if ("update".equals(method)) {
      EventResource eventResource = new EventResource(id, title, date, ticketPrice);
      EventResource result = facade.updateEvent(eventResource);
      modelAndView.addObject(ATTRIBUTE_EVENT, result);
    }
    return modelAndView;
  }
}
