package com.epam.jmp.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class HomeController {
  @GetMapping("/home")
  public String home() {
    return "index.html";
  }

  @GetMapping("/goToTicket")
  public String ticket() {
    return "ticket.html";
  }

  @GetMapping("/goToUser")
  public String user() {
    return "user.html";
  }

  @GetMapping("/goToEvent")
  public String event() {
    return "event.html";
  }

  @ModelAttribute("categories")
  public List<String> getCategories() {
    return List.of("Premium", "Standard", "Bar");
  }
}
