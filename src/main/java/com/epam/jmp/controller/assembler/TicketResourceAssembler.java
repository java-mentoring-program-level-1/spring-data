package com.epam.jmp.controller.assembler;

import com.epam.jmp.model.event.Event;
import com.epam.jmp.model.event.EventImpl;
import com.epam.jmp.model.ticket.Ticket;
import com.epam.jmp.model.ticket.TicketImpl;
import com.epam.jmp.model.ticket.TicketResource;
import com.epam.jmp.model.user.User;
import com.epam.jmp.model.user.UserImpl;

public class TicketResourceAssembler {
  public TicketResource toResource(Ticket entity) {
    return new TicketResource(entity.getId(), entity.getUser().getId(),
      entity.getEvent().getId(), entity.getCategory(), entity.getPlace());
  }

  public TicketImpl toEntity(TicketResource resource) {
    TicketImpl ticket = new TicketImpl();
    ticket.setId(resource.getId());
    ticket.setCategory(resource.getCategory());
    ticket.setPlace(resource.getPlace());

    Event event = new EventImpl();
    event.setId(resource.getEventId());
    ticket.setEvent(event);

    User user = new UserImpl();
    user.setId(resource.getUserId());
    ticket.setUser(user);

    return ticket;
  }
}
