package com.epam.jmp.controller.assembler;

import com.epam.jmp.model.event.Event;
import com.epam.jmp.model.event.EventImpl;
import com.epam.jmp.model.event.EventResource;

public class EventResourceAssembler {

  public EventResource toResource(Event input) {
    return new EventResource(input.getId(), input.getTitle(), input.getDate(), input.getTicketPrice());
  }

  public EventImpl toEntity(EventResource eventResource) {
    EventImpl event = new EventImpl();
    event.setId(eventResource.getId());
    event.setDate(eventResource.getDate());
    event.setTitle(eventResource.getTitle());
    event.setTicketPrice(eventResource.getTicketPrice());
    return event;
  }
}
