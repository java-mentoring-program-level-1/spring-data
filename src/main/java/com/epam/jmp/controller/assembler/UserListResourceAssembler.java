package com.epam.jmp.controller.assembler;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

import com.epam.jmp.model.user.UserImpl;
import com.epam.jmp.model.user.UserListResource;
import com.epam.jmp.model.user.UserResource;

public class UserListResourceAssembler extends UserResourceAssembler {
  public UserListResource toResources(Page<UserImpl> userPage) {
    List<UserResource> userResources = userPage.getContent().stream()
      .map(this::toResource)
      .collect(Collectors.toList());
    return new UserListResource(userResources, userPage.getNumber(), userPage.getSize(), userPage.getTotalPages(), userPage.getTotalElements());
  }
}
