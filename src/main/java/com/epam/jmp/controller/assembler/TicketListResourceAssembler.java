package com.epam.jmp.controller.assembler;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

import com.epam.jmp.model.ticket.Ticket;
import com.epam.jmp.model.ticket.TicketImpl;
import com.epam.jmp.model.ticket.TicketListResource;
import com.epam.jmp.model.ticket.TicketResource;

public class TicketListResourceAssembler extends TicketResourceAssembler {
  public TicketListResource toResources(Page<TicketImpl> ticketPage) {
    List<TicketResource> ticketResources = ticketPage.getContent().stream()
      .map(this::toResource)
      .collect(Collectors.toList());

    return new TicketListResource(ticketResources, ticketPage.getNumber(),
      ticketPage.getSize(), ticketPage.getTotalPages(), ticketPage.getTotalElements());
  }
}
