package com.epam.jmp.controller.assembler;

import com.epam.jmp.model.user.User;
import com.epam.jmp.model.user.UserImpl;
import com.epam.jmp.model.user.UserResource;
import com.epam.jmp.model.user.account.UserAccount;

public class UserResourceAssembler {
  public UserResource toResource(User entity) {
    return new UserResource(entity.getId(), entity.getName(),
      entity.getEmail());
  }

  public UserImpl toEntity(UserResource resource) {
    UserImpl user = new UserImpl();
    user.setId(resource.getId());
    user.setName(resource.getName());
    user.setEmail(resource.getEmail());
    return user;
  }
}
