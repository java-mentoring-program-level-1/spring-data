package com.epam.jmp.controller.assembler;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

import com.epam.jmp.model.event.EventImpl;
import com.epam.jmp.model.event.EventListResource;
import com.epam.jmp.model.event.EventResource;

public class EventListResourceAssembler extends EventResourceAssembler {

  public EventListResource toResources(Page<EventImpl> eventPage) {
    List<EventResource> eventResources = eventPage.getContent()
      .stream()
      .map(super::toResource)
      .collect(Collectors.toList());
    return new EventListResource(eventResources, eventPage.getNumber(),
      eventPage.getSize(), eventPage.getTotalPages(), eventPage.getTotalElements());
  }
}
