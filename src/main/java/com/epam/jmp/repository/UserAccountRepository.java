package com.epam.jmp.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.epam.jmp.model.user.account.UserAccountImpl;

@Repository
public interface UserAccountRepository extends CrudRepository<UserAccountImpl, Long> {
  Optional<UserAccountImpl> findByUserId(long userId);
  void deleteByUserId(long userId);
  boolean existsByUserId(long userId);
}
