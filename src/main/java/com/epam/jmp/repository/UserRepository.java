package com.epam.jmp.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.jmp.model.user.UserImpl;

@Repository
public interface UserRepository extends JpaRepository<UserImpl, Long> {
  Optional<UserImpl> findByEmail(String email);
  Page<UserImpl> findAllByName(String name, Pageable pageable);
  boolean existsById(long id);
}
