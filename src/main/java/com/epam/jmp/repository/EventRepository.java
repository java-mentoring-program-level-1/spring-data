package com.epam.jmp.repository;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.jmp.model.event.EventImpl;

@Repository
public interface EventRepository extends JpaRepository<EventImpl, Long> {
	Page<EventImpl> findAllByTitle(String title, Pageable pageable);
	Page<EventImpl> findAllByDateBetween(Date start, Date end, Pageable pageable);
	boolean existsById(long id);
}
