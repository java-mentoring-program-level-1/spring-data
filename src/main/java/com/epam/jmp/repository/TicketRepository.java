package com.epam.jmp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.jmp.model.event.Event;
import com.epam.jmp.model.ticket.TicketImpl;
import com.epam.jmp.model.user.User;

@Repository
public interface TicketRepository extends JpaRepository<TicketImpl, Long> {
	Page<TicketImpl> findByUser(User user, Pageable pageable);
	Page<TicketImpl> findByEvent(Event event, Pageable pageable);
	
}
