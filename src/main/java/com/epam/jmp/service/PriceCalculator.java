package com.epam.jmp.service;

import java.math.BigDecimal;

import com.epam.jmp.model.ticket.Ticket;

public interface PriceCalculator {
  BigDecimal calculate(BigDecimal amount, Ticket.Category category);
}
