package com.epam.jmp.service;

import java.io.ByteArrayInputStream;
import java.util.List;

public interface PDFGenerator<T> {
  ByteArrayInputStream generate(List<T> objects);
}
