package com.epam.jmp.service.impl;

import java.math.BigDecimal;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import com.epam.jmp.model.exception.EntityNotFoundException;
import com.epam.jmp.model.user.UserImpl;
import com.epam.jmp.model.user.account.UserAccountImpl;
import com.epam.jmp.repository.UserRepository;
import com.epam.jmp.service.UserAccountService;
import com.epam.jmp.service.UserService;

@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {
	private UserRepository userRepository;
	private UserAccountService accountService;

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Transactional
	public UserImpl create(UserImpl user) {
		UserImpl result = userRepository.save(user);
		UserAccountImpl account = new UserAccountImpl();
		account.setUser(result);
		accountService.create(account);
		return result;
	}

	@Override
	public UserImpl findById(long id) {
		return userRepository.findById(id).orElseThrow(() -> {
			throw new EntityNotFoundException("User with id = " + id + " not found");
		});
	}

  @Transactional
	@Override
	public UserImpl update(UserImpl user) {
		return userRepository.save(user);
	}

  @Transactional
	@Override
	public boolean deleteById(long id) {
		boolean exist = userRepository.existsById(id);
		if(exist) {
			accountService.deleteByUserId(id);
			userRepository.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public Page<UserImpl> findByName(String name, Pageable pageable) {
		Page<UserImpl> userPage = userRepository.findAllByName(name, pageable);
		if (userPage.isEmpty()) {
			throw new EntityNotFoundException("User(s) with name = " + name + " is/are not found");
		}
		return userPage;
	}

	@Override
	public UserImpl findByEmail(String email) {
		return userRepository.findByEmail(email).orElseThrow(() -> {
			throw new EntityNotFoundException("User with Email: " + email + " not found");
		});
	}

	@Transactional
	@Override
	public void withdrawMoneyByUserId(long userId, BigDecimal amount) {
		accountService.withdraw(userId, amount);
	}

	public UserAccountService getAccountService() {
		return accountService;
	}

	public void setAccountService(UserAccountService accountService) {
		this.accountService = accountService;
	}
}
