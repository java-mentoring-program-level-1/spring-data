package com.epam.jmp.service.impl;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.transaction.annotation.Transactional;

import com.epam.jmp.model.exception.EntityNotFoundException;
import com.epam.jmp.model.user.account.UserAccount;
import com.epam.jmp.model.user.account.UserAccountImpl;
import com.epam.jmp.repository.UserAccountRepository;
import com.epam.jmp.service.UserAccountService;

@Transactional(readOnly = true)
public class UserAccountServiceImpl implements UserAccountService {

  private final UserAccountRepository accountRepository;

  public UserAccountServiceImpl(UserAccountRepository userAccountRepository) {
    this.accountRepository = userAccountRepository;
  }

  @Transactional
  @Override
  public UserAccountImpl create(UserAccountImpl account) {
    return accountRepository.save(account);
  }

  @Transactional
  @Override
  public void withdraw(long userId, BigDecimal amount) {
    Optional<UserAccountImpl> accountOptional = accountRepository.findByUserId(userId);
    UserAccount account = accountOptional.orElseThrow(() -> new EntityNotFoundException(
      "Account with user id = " + userId + " not found"));
    BigDecimal remain = account.getAmount().subtract(amount);
    if (remain.compareTo(BigDecimal.ZERO) <= 0) {
      throw new IllegalStateException("User account has no enough money");
    }
    account.setAmount(remain);
  }

  @Override
  public UserAccountImpl findByUserId(long userId) {
    return accountRepository.findByUserId(userId).orElseThrow(() -> {
      throw new EntityNotFoundException("Account for user with id = " + userId + " not found");
    });
  }

  @Transactional
  @Override
  public boolean deleteByUserId(long userId) {
    accountRepository.deleteByUserId(userId);
    return !accountRepository.existsByUserId(userId);
  }

}
