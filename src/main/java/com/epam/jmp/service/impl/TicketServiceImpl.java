package com.epam.jmp.service.impl;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import com.epam.jmp.model.event.Event;
import com.epam.jmp.model.exception.EntityNotFoundException;
import com.epam.jmp.model.ticket.TicketImpl;
import com.epam.jmp.model.user.User;
import com.epam.jmp.repository.TicketRepository;
import com.epam.jmp.service.EventService;
import com.epam.jmp.service.PDFGenerator;
import com.epam.jmp.service.PriceCalculator;
import com.epam.jmp.service.TicketService;
import com.epam.jmp.service.UserService;

@Transactional(readOnly = true)
public class TicketServiceImpl implements TicketService {
  private TicketRepository ticketRepository;
  private PDFGenerator<TicketImpl> ticketPDFGenerator;
  private UserService userService;
  private EventService eventService;
  private PriceCalculator priceCalculator;

  public TicketServiceImpl(TicketRepository ticketRepository, PDFGenerator<TicketImpl> ticketPDFGenerator,
    UserService userService, EventService eventService,
    PriceCalculator priceCalculator) {

    this.ticketRepository = ticketRepository;
    this.ticketPDFGenerator = ticketPDFGenerator;
    this.userService = userService;
    this.eventService = eventService;
    this.priceCalculator = priceCalculator;
  }

  @Override
  public Page<TicketImpl> findByUser(User user, Pageable pageable) {
    Page<TicketImpl> ticketPage = ticketRepository.findByUser(user, pageable);
    if(ticketPage.isEmpty()) {
      throw new EntityNotFoundException("Ticket(s) with userId = " + user.getId() + " is/are not found");
    }
    return ticketPage;
  }

  @Override
  public Page<TicketImpl> findByEvent(Event event, Pageable pageable) {
    Page<TicketImpl> ticketPage = ticketRepository.findByEvent(event, pageable);
    if(ticketPage.isEmpty()) {
      throw new EntityNotFoundException("Ticket(s) with eventId = " + event.getId() + " is/are not found");
    }
    return ticketPage;
  }

  @Transactional
  @Override
  public TicketImpl book(TicketImpl ticket) {
      Event event = eventService.findById(ticket.getEvent().getId());
      BigDecimal ticketPrice = priceCalculator.calculate(event.getTicketPrice(), ticket.getCategory());
      userService.withdrawMoneyByUserId(ticket.getUser().getId(), ticketPrice);
      try {
        ticketRepository.saveAndFlush(ticket);
      } catch (DataIntegrityViolationException ex) {
        throw new IllegalArgumentException(ex);
      }
      return ticket;
  }

  @Transactional
  @Override
  public boolean cancel(long id) {
    boolean exist = ticketRepository.existsById(id);
    if(exist) {
      ticketRepository.deleteById(id);
      return true;
    }
    return false;
  }

  @Override
  public ByteArrayInputStream generatePDFReport(List<TicketImpl> tickets) {
    return ticketPDFGenerator.generate(tickets);
  }

}
