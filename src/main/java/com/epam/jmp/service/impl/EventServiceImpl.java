package com.epam.jmp.service.impl;

import java.util.Calendar;
import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import com.epam.jmp.model.event.EventImpl;
import com.epam.jmp.model.exception.EntityNotFoundException;
import com.epam.jmp.repository.EventRepository;
import com.epam.jmp.service.EventService;

@Transactional(readOnly = true)
public class EventServiceImpl implements EventService {
  private EventRepository repository;

  @Transactional
  @Override
  public EventImpl create(EventImpl event) {
    return repository.save(event);
  }

  @Override
  public EventImpl findById(long id) {
    return repository.findById(id).orElseThrow(() -> {
      throw new EntityNotFoundException("Event with ID: " + id + " not found");
    });
  }

  @Transactional
  @Override
  public EventImpl update(EventImpl event) {
    return repository.save(event);
  }

  @Transactional
  @Override
  public boolean deleteById(long id) {
    boolean exist = repository.existsById(id);
    if(exist) {
      repository.deleteById(id);
      return true;
    }
    return false;
  }

  @Override
  public Page<EventImpl> findByTitle(String title, Pageable pageable) {
    Page<EventImpl> events = repository.findAllByTitle(title, pageable);

    if (events.isEmpty()) {
      throw new EntityNotFoundException("Event(s) with title = " + title + " is/are not found");
    }
    return events;
  }

  @Override
  public Page<EventImpl> findForDate(Date day, Pageable pageable) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(day);
    calendar.add(Calendar.HOUR_OF_DAY, 23);
    calendar.add(Calendar.MINUTE, 59);
    Date endDay = calendar.getTime();
    Page<EventImpl> events = repository.findAllByDateBetween(day, endDay, pageable);

    if (events.isEmpty()) {
      throw new EntityNotFoundException("Event(s) with date = " + day + " is/are not found");
    }
    return events;
  }

  public EventRepository getRepository() {
    return repository;
  }

  public void setRepository(EventRepository repository) {
    this.repository = repository;
  }

}
