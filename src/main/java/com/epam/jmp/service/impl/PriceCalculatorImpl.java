package com.epam.jmp.service.impl;

import java.math.BigDecimal;

import com.epam.jmp.model.ticket.Ticket.Category;
import com.epam.jmp.service.PriceCalculator;

public class PriceCalculatorImpl implements PriceCalculator {
  @Override
  public BigDecimal calculate(BigDecimal amount, Category category) {
    if (category == Category.BAR) {
      return amount.multiply(new BigDecimal("3.5"));
    }
    else if (category == Category.PREMIUM) {
      return amount.multiply(new BigDecimal("2.5"));
    }
    return amount.multiply(new BigDecimal("1.5"));
  }
}
