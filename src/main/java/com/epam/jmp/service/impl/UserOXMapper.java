package com.epam.jmp.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.transform.stream.StreamSource;

import org.springframework.core.io.ClassPathResource;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

import com.epam.jmp.model.user.UserContainer;
import com.epam.jmp.service.OXMapper;

public class UserOXMapper implements OXMapper<UserContainer> {
  private String filePath;
  private Marshaller marshaller;
  private Unmarshaller unmarshaller;

  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public Marshaller getMarshaller() {
    return marshaller;
  }

  public void setMarshaller(Marshaller marshaller) {
    this.marshaller = marshaller;
  }

  public Unmarshaller getUnmarshaller() {
    return unmarshaller;
  }

  public void setUnmarshaller(Unmarshaller unmarshaller) {
    this.unmarshaller = unmarshaller;
  }

  private File getFile(String path) throws IOException {
    return new ClassPathResource(path).getFile();
  }

  @Override
  public UserContainer xmlToObject() throws IOException {
    try(FileInputStream is = new FileInputStream(getFile(filePath))) {
      UserContainer unmarshal = (UserContainer) this.unmarshaller.unmarshal(new StreamSource(is));
      return unmarshal;
    }
  }
}
