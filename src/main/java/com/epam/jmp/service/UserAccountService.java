package com.epam.jmp.service;

import java.math.BigDecimal;

import com.epam.jmp.model.user.account.UserAccountImpl;

public interface UserAccountService {
  UserAccountImpl create(UserAccountImpl account);
  void withdraw(long userId, BigDecimal amount);
  UserAccountImpl findByUserId(long userId);
  boolean deleteByUserId(long id);
}
