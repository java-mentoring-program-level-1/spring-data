package com.epam.jmp.service;

import java.io.IOException;

public interface OXMapper<T> {
  T xmlToObject() throws IOException;
}
