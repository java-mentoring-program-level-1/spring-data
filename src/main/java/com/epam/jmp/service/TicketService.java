package com.epam.jmp.service;

import java.io.ByteArrayInputStream;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.epam.jmp.model.event.Event;
import com.epam.jmp.model.ticket.Ticket;
import com.epam.jmp.model.ticket.TicketImpl;
import com.epam.jmp.model.user.User;

public interface TicketService {
	Page<TicketImpl> findByUser(User user, Pageable pageable);
	Page<TicketImpl> findByEvent(Event event, Pageable pageable);
	TicketImpl book(TicketImpl ticket);
	boolean cancel(long id);
	ByteArrayInputStream generatePDFReport(List<TicketImpl> tickets);
//	void preloadTickets();
}
