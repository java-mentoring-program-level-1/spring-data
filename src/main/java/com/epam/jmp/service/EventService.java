package com.epam.jmp.service;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.epam.jmp.model.event.EventImpl;

public interface EventService {
  EventImpl create(EventImpl event);
  EventImpl findById(long id);
  EventImpl update(EventImpl event);
  boolean deleteById(long eventId);
  Page<EventImpl> findByTitle(String title, Pageable pageable);
  Page<EventImpl> findForDate(Date day, Pageable pageable);

//  void preloadEvents();
}
