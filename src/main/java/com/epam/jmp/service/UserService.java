package com.epam.jmp.service;

import java.math.BigDecimal;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.epam.jmp.model.user.UserImpl;

public interface UserService {
	UserImpl create(UserImpl user);
	UserImpl findById(long userId);
	UserImpl update(UserImpl user);
	boolean deleteById(long userId);
	Page<UserImpl> findByName(String name, Pageable pageable);
	UserImpl findByEmail(String email);
	void withdrawMoneyByUserId(long userId, BigDecimal amount);
}
